xquery version "3.1";
(: call this script after initialization of the db :)

let $login := xmldb:login("/db/sade-projects/textgrid/", "admin", "")

let $configdoc := doc( "/db/sade-projects/textgrid/config.xml" )

let $credentials := (
    update replace $configdoc//param[@key="textgrid.user"]/text() with text {"TODO"},
    update replace $configdoc//param[@key="textgrid.password"]/text() with text {"TODO"},
    update replace $configdoc//param[@key="secret"]/text() with text {"TODO"},
    update replace $configdoc//param[@key="sade.password"]/text() with text {"TODO"},
    update replace $configdoc//param[@key="dokuwiki.user"]/text() with text {"TODO"},
    update replace $configdoc//param[@key="dokuwiki.password"]/text() with text {"TODO"})

let $package-urls := ("http://exist-db.org/apps/eXide", "http://exist-db.org/apps/existdb-packageservice", "http://exist-db.org/apps/fundocs", "http://exist-db.org/apps/usermanager")
let $remove-packages :=
  if(environment-variable("CI_COMMIT_REF_NAME") ne "main") then false() else 
    for $package-url in $package-urls
    return
        if ($package-url = repo:list()) then
            let $undeploy := repo:undeploy($package-url)
            let $remove := repo:remove($package-url)
            return
                $remove
        else
            false()

return
    "done."
