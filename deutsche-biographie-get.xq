xquery version "3.1";

let $urls := ("http://www.historische-kommission-muenchen-editionen.de/beacon_db_register.txt",
              "http://www.historische-kommission-muenchen-editionen.de/beacon_ndb.txt",
              "http://www.historische-kommission-muenchen-editionen.de/beacon_adb.txt")

let $maps :=
for $url in $urls
let $request := <hc:request method="get" href="{$url}" />
let $response := hc:send-request($request)
let $beacon := $response[2] => xmldb:decode() => tokenize("&#x0A;")
let $target := $beacon[starts-with(., "#TARGET:")] => substring-after(": ")
  (: should return something like "http://www.deutsche-biographie.de/pnd{ID}.html#adbcontent"
   : where «{ID}» is to be replaced by the GND ID :)
  let $replacement := "\{ID\}"
  return map:merge(
                for $i in $beacon[not(starts-with(., "#"))][not(starts-with(., "("))][. != ""]
                return
                  map:entry($i, replace($target, $replacement, $i))
  )
let $merge := map:merge($maps)

return
<deutsche-biographie dateTime="{current-dateTime()}">{
    for $key in map:keys($merge)
    order by $key
    return
        <gnd xml:id="gnd{$key}" db-url="{$merge($key)}"/>
}</deutsche-biographie> ! xmldb:store("/db", "deutsche-biographie.xml", .)
