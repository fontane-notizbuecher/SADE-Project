xquery version "3.1";

declare variable $start := "/db";

declare function local:recursive($param) {
    for $col in xmldb:get-child-collections($param)
    let $path := $param || "/" || $col
    return
        (
            local:recursive($path),
            xmldb:get-child-resources($path) ! ( $path || "/" ||  .)
        )
};

for $i in local:recursive($start)
    let $tok := tokenize( $i, "/" )
    let $tokCnt := count($tok)
    let $datTime := xmldb:last-modified(string-join($tok[ position() != last() ], "/"), $tok[last()])
    where not( contains( $i, "data/xml/" ) )
    order by $datTime descending
return
    (
        $i,
        $datTime
    )
