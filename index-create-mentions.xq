xquery version "3.1";
declare namespace tei="http://www.tei-c.org/ns/1.0";

declare function local:remove-old-stuff($dataPath as xs:string){
    update delete collection($dataPath)//tei:link[@corresp="https://schema.org/mentions"],
    update delete collection($dataPath)//tei:linkGrp[not(.//*)]
};

declare function local:remove-unused-items($dataPath as xs:string) {
  let $allTargets :=
    collection($dataPath)
      //tei:link/@target/tokenize(., " ")
      [contains(., ":")]/substring-after(., ":")
  let $allItems := (
    collection($dataPath)//tei:body//tei:item[not(.//tei:link[@corresp="https://schema.org/mentions"])]/*[1],
    collection($dataPath)//tei:body//tei:place[not(.//tei:link[@corresp="https://schema.org/mentions"])]/*[1],
    collection($dataPath)//tei:body//tei:person[not(.//tei:link[@corresp="https://schema.org/mentions"])]/*[1],
    collection($dataPath)//tei:body//tei:persGrp[not(.//tei:link[@corresp="https://schema.org/mentions"])]/*[1],
    collection($dataPath)//tei:body//tei:event[not(.//tei:link[@corresp="https://schema.org/mentions"])]/*[1]
    )
  return
    for $i in $allItems
    return
      update delete $i/parent::*[@xml:id != $allTargets]
};

declare function local:xpath($rs as element(tei:rs))
as xs:string{
let $components :=
    for $mention in $rs/ancestor-or-self::tei:*
    let $name := $mention/local-name()
    let $count :=
        if($name != "TEI")
        then count( $mention/preceding-sibling::tei:*[local-name() = $name] ) + 1
        else 0
    return
        switch ($name)
            case "surface" return
                let $n := $mention/string(@n)
                return
                    if($n != "")
                    then $name || "[@n='" || $n || "']"
                    else $name || (if($count != 0) then "[" || $count || "]" else "")
            default return
                $name || (if($count != 0) then "[" || $count || "]" else "")
return
    string-join($components, "/")
};

let $dataPath := "/db/sade-projects/textgrid/data/xml/data/"
let $cleanup := local:remove-old-stuff($dataPath)

let $prefixes := (//tei:listPrefixDef[(.//@replacementPattern/contains(., "textgrid:")) = true()])/tei:prefixDef
let $map :=
    map:merge(
        $prefixes !
        map:entry(string(./@ident), $dataPath || substring(./@replacementPattern, 10, 5) || ".xml")
    )
let $refs :=
    collection($dataPath)//tei:sourceDoc//tei:rs/@ref[starts-with(., map:keys($map))][./base-uri() != $map?*]/tokenize(., " ")[. != ""]
    => distinct-values()

return (
    for $ref in $refs
    let $prefix := substring-before($ref, ":")
    let $id := substring-after($ref, ":")

    let $doc := doc( $map($prefix) )
    let $node := $doc/id($id)

    let $mentions := collection($dataPath)//tei:sourceDoc//tei:rs[contains(./@ref, $ref)]
    let $xpathes :=
        for $mention in $mentions
        let $uri := $mention/base-uri() => substring-before(".xml") => substring-after("/xml/data/")
        return
            [ $uri , local:xpath($mention) ]

    let $links :=
        for $xpath in $xpathes
        return
            element tei:link {
                attribute corresp {"https://schema.org/mentions"},
                attribute target {"http://textgridrep.org/textgrid:"|| $xpath?(1) || "#xpath(" || $xpath?(2),
                                    "#" || $id}
            }
    let $linkGrp := element tei:linkGrp {
        $links
    }
    where count($node) = 1
    return
        let $do :=
        if($node/tei:linkGrp)
        then
            update insert $links into $node/tei:linkGrp
        else
            update insert $linkGrp into $node
        return
            $node
    ,
    local:remove-unused-items($dataPath)
)
