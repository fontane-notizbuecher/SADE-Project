xquery version "3.1";

declare namespace tei="http://www.tei-c.org/ns/1.0";

declare function local:is-related($rs as element(tei:rs), $xmlid as xs:string)
as xs:boolean{
    let $refs := (tokenize($rs/@ref, " ")[. != ""] ! substring-after(., ":"))
    return
        $xmlid = $refs
};

declare function local:xpath($rs as element(tei:rs))
as xs:string{
let $components :=
    for $mention in $rs/ancestor-or-self::tei:*
    let $name := $mention/local-name()
    let $count :=
        if($name != "TEI")
        then count( $mention/preceding-sibling::tei:*[local-name() = $name] ) + 1
        else 0
    return
        switch ($name)
            case "surface" return
                let $n := $mention/string(@n)
                return
                    if($n != "")
                    then $name || "[@n='" || $n || "']"
                    else $name || (if($count != 0) then "[" || $count || "]" else "")
            default return
                $name || (if($count != 0) then "[" || $count || "]" else "")
let $ref := "[@ref='"||string($rs/@ref)||"']"
return
    string-join($components, "/") || $ref
};

let $dataPath := "/db/sade-projects/textgrid/data/xml/data/"

(: remove all mentions, we want to recreate them :)
let $cleanup := update delete //tei:link[@corresp="https://schema.org/mentions"]
let $cleanup := update delete //tei:linkGrp//comment()[contains(., "Notizbuch ")]

(: map prefixes to index documents :)
let $prefixes := (//tei:listPrefixDef[(.//@replacementPattern/contains(., "textgrid:")) = true()])/tei:prefixDef
let $map :=
    map:merge(
        $prefixes !
        map:entry(string(./@ident), $dataPath || substring(./@replacementPattern, 10, 5) || ".xml")
    )

(: collect available entities from the index :)
let $ids := $map?* ! doc(.)//tei:body//*[@xml:id]

for $id in $ids
let $xmlid := string($id/@xml:id)
let $refs := collection($dataPath)//tei:sourceDoc//tei:rs[contains(./@ref, $xmlid)][not(@prev)][local:is-related(., $xmlid)]
return
    let $xpathes :=
        for $mention in $refs
        let $uri := $mention/base-uri() => substring-before(".xml") => substring-after("/xml/data/")
        let $title := string(($mention/root()//tei:title)[1])
        return
            [ $uri, local:xpath($mention), $title ]

    let $links :=
        for $xpath in $xpathes
        return
            (element tei:link {
                attribute corresp {"https://schema.org/mentions"},
                attribute target {"http://textgridrep.org/textgrid:"|| $xpath?(1) || "#xpath(" || $xpath?(2),
                                    "#" || $xmlid}
            },
            comment {$xpath?(3)},
            text {"&#10;  "})
    let $linkGrp := element tei:linkGrp {
        $links
    }

    let $do :=
    if(empty($links)) then () else
        if($id/tei:linkGrp)
        then
            update insert $links into $id/tei:linkGrp
        else
            update insert $linkGrp into $id
return
    $linkGrp
