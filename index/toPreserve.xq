xquery version "3.1";

declare namespace tei="http://www.tei-c.org/ns/1.0";

declare function local:target($link) {
    let $tokens := tokenize(replace($link, "#|\w{3}:", ""), " ")
    return
        $tokens[not(starts-with(., "h//textgridrep.org"))]
};

let $dataPath := "/db/sade-projects/textgrid/data/xml/data/"
let $collection := collection($dataPath)

(: map prefixes to index documents :)
let $prefixes := ($collection//tei:listPrefixDef[(.//@replacementPattern/contains(., "textgrid:")) = true()])/tei:prefixDef
let $map :=
    map:merge(
        $prefixes !
        map:entry(string(./@ident), $dataPath || substring(./@replacementPattern, 10, 5) || ".xml")
    )

(: collect available entities from the index :)
let $ids := $map?* ! doc(.)//tei:body//*[@xml:id]


let $rss := $collection//tei:rs[@ref]
let $direct := ($rss/@ref/string() ! local:target(.)) => distinct-values()

let $direct-items := for $id in $direct return $ids[@xml:id = $id]

let $indirect :=
    (for $item in $direct-items
    let $xmlid := $item/@xml:id => string()
    let $links := $item//tei:link[@corresp][not(@corresp="https://schema.org/mentions")]
    return
        let $targets := ($links ! local:target(string(./@target))) => distinct-values()
        return
            $targets
    ) => distinct-values()
let $indirect-items := for $id in $indirect return $ids[@xml:id = $id]

let $toPreserve := distinct-values(($direct, $indirect))

return
    $toPreserve
