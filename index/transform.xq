xquery version "3.1";

declare namespace tei="http://www.tei-c.org/ns/1.0";

declare variable $toPreserve := util:eval(xs:anyURI("toPreserve.xq"));
declare variable $dataPath := "/db/sade-projects/textgrid/data/xml/data/";
declare variable $collection := collection($dataPath);

declare function local:is-related($rs as element(tei:rs), $xmlid as xs:string)
as xs:boolean{
    let $refs := (tokenize($rs/@ref, " ")[. != ""] ! substring-after(., ":"))
    return
        $xmlid = $refs
};

declare function local:links($xmlid) {
    let $refs := collection($dataPath)//tei:sourceDoc//tei:rs[contains(./@ref, $xmlid)][not(@prev)][local:is-related(., $xmlid)]
    let $xpathes :=
        for $mention in $refs
        let $uri := $mention/base-uri() => substring-before(".xml") => substring-after("/xml/data/")
        let $title := string(($mention/root()//tei:title)[1])
        return
            [ $uri, local:xpath($mention), $title ]
    let $links :=
        for $xpath in $xpathes
        return
            (element tei:link {
                attribute corresp {"https://schema.org/mentions"},
                attribute target {"http://textgridrep.org/textgrid:"|| $xpath?(1) || "#xpath(" || $xpath?(2),
                                    "#" || $xmlid}
            },
            comment {$xpath?(3)},
            text {"&#10;  "})
return
    $links
};

declare function local:transform($nodes as node()*) {
for $node in $nodes
    return
        typeswitch ( $node )
        case element(tei:teiHeader) return $node
        case element(tei:linkGrp) return
            if(not($node/*)) then () else
            element {$node/node-name()} {
                $node/@*,
                local:transform($node/node())
            }
        case element(tei:note) return
            if(not($node/text())) then () else
            $node

        case element(tei:event) return
            local:feenstaub($node)
        case element(tei:item) return
            if($node/parent::tei:list/parent::tei:body)
            then
                element {$node/node-name()} {
                    $node/@*,
                    local:transform($node/node())
                }
            else local:feenstaub($node)
        case element(tei:org) return
            local:feenstaub($node)
        case element(tei:person) return
            local:feenstaub($node)
        case element(tei:personGrp) return
            local:feenstaub($node)
        case element(tei:place) return
            local:feenstaub($node)

        case processing-instruction() return $node
        case comment() return ()
        case text() return
            normalize-space($node)
        default return
            element {$node/node-name()} {
                $node/@*,
                local:transform($node/node())
            }
};

declare function local:feenstaub($node as element()) {
let $links := local:links(string($node/@xml:id))
let $moreLinks := $node/tei:linkGrp/*[@corresp!="https://schema.org/mentions"]
return
    if(count(($links, $moreLinks)) = 0) then () else
    if(not(($node//@xml:id/string()) = $toPreserve)) then () else
    element {$node/node-name()} {
        $node/@*,
        local:transform($node/node()[local-name() != "linkGrp"]),
        element tei:linkGrp {
            $moreLinks,
            $links
        }
    }
};

declare function local:xpath($rs as element(tei:rs))
as xs:string{
let $components :=
    for $mention in $rs/ancestor-or-self::tei:*
    let $name := $mention/local-name()
    let $count :=
        if($name != "TEI")
        then count( $mention/preceding-sibling::tei:*[local-name() = $name] ) + 1
        else 0
    return
        switch ($name)
            case "surface" return
                let $n := $mention/string(@n)
                return
                    if($n != "")
                    then $name || "[@n='" || $n || "']"
                    else $name || (if($count != 0) then "[" || $count || "]" else "")
            default return
                $name || (if($count != 0) then "[" || $count || "]" else "")
let $ref := "[@ref='"||string($rs/@ref)||"']"
return
    (string-join($components, "/") || $ref)
    => replace("TEI/sourceDoc\[1\]/", "//")
};

let $login := xmldb:login("/db", "admin", "")

(: map prefixes to index documents :)
let $prefixes := ($collection//tei:listPrefixDef[(.//@replacementPattern/contains(., "textgrid:")) = true()])/tei:prefixDef
let $map :=
    map:merge(
        $prefixes !
        map:entry(string(./@ident), $dataPath || substring(./@replacementPattern, 10, 5) || ".xml")
    )

return
    for $docPath in $map?*
    let $filename := ($docPath => tokenize("/"))[last()]
    let $doc := doc($docPath)
    let $transform := document {local:transform($doc/node())}
    let $do := xmldb:store($dataPath, $filename, $transform)
    return
        true()
