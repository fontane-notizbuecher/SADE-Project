xquery version "3.1";

(: The following external variables are set by the repo:deploy function :)
(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;

let $fileSeparator := util:system-property("file.separator")

return (
    xmldb:create-collection("/db", "sade-projects"),
    xmldb:store-files-from-pattern(
        (: target collection  :)    "/db/sade-projects",
        (: source directory   :)    $dir || $fileSeparator || "sade-projects",
        (: What to ingest     :)    "**/*",
        (: MIME type :)             (),
        (: preserve structure :)    true()
    ),
    (collection("/db/sade-projects")/base-uri()[ends-with(., ".xq") or ends-with(., ".xql")] ! sm:chmod(., "rwxrwxr-x")),
    file:delete($dir || $fileSeparator || "sade-projects"),

    xmldb:store-files-from-pattern(
        (: target collection  :)    "/db/system",
        (: source directory   :)    $dir || $fileSeparator || "system",
        (: What to ingest     :)    "**/*",
        (: MIME type :)             (),
        (: preserve structure :)    true()
    ),
    file:delete($dir || $fileSeparator || "system"),

    util:log-system-out("fontane: pre-install finished")
)