xquery version "3.1";

(:~
 : Script to be executed during initialization of the db, when no password is
 : set. It will create a cache file for the index view, so a complete instance
 : is required.
 : @author Mathias Göbel
:)

import module namespace f-indexviewer="http://fontane-nb.dariah.eu/index-viewer" at "/db/apps/SADE/modules/fontane/index-viewer.xqm";

declare option exist:output-size-limit "3000000";

xmldb:login("/db", "admin", ""),
f-indexviewer:prepare-new-cache()//*:li[@id] => count()
