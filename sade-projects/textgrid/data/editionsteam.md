# Editionsteam

### Leitung

* Gesamtleitung, Idee, Konzeption und Herausgeberin: [Dr. Gabriele Radecke](http://www.uni-goettingen.de/de/110612.html) (Theodor Fontane-Arbeitsstelle, Universität Göttingen, Seminar für Deutsche Philologie)

* Informationswissenschaftliche und -technologische Leitung sowie Koordination in der SUB: [Dr. Mirjam Blümm](http://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/mirjam-bluemm/) (Niedersächsische Staats- und Universitätsbibliothek Göttingen, Abteilung Forschung & Entwicklung)

* Informationswissenschaftliche und -technologische Leitung sowie Koordination in der SUB von Juni 2011 bis März 2015: [Prof. Dr. Heike Neuroth](http://www.fh-potsdam.de/person/person-action/heike-neuroth/show/Person/) (Niedersächsische Staats- und Universitätsbibliothek Göttingen, Abteilung Forschung & Entwicklung)

### Mitarbeiterinnen und Mitarbeiter

* Edition: [Dr. Gabriele Radecke](http://www.uni-goettingen.de/de/110612.html) (Theodor Fontane-Arbeitsstelle, Universität Göttingen, Seminar für Deutsche Philologie)





* Editorische Assistenz:  Judith Michaelis, [Rahel Rami](http://www.uni-goettingen.de/de/485399.html) und Hartmut Hombrecher (seit August 2015) (Theodor Fontane-Arbeitsstelle, Universität Göttingen, Seminar für Deutsche Philologie)

* Metadaten: [Martin de la Iglesia](http://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/martin-de-la-iglesia/) (Niedersächsische Staats- und Universitätsbibliothek Göttingen, Gruppe Metadaten und Datenkonversion)

* IT, Visualisierung und Portalentwicklung: [Mathias Göbel](http://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/mathias-goebel/) (Niedersächsische Staats- und Universitätsbibliothek Göttingen, Abteilung Forschung & Entwicklung)


### Beratung

Christiane Fritze, Stefan Funk, Alexander Jahnke, Felix Lohmeier, Sibylle Söring, Ubbo Veentjer und Thorsten Vitt



