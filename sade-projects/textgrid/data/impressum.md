
# IMPRESSUM

Das Projekt „Genetisch-kritische und kommentierte Hybrid-Edition von Theodor Fontanes Notizbüchern basierend auf einer Virtuellen Forschungsumgebung“ ist ein von der Deutschen Forschungsgemeinschaft (DFG) gefördertes Forschungsprojekt (Projekt-Nummern: NE 1434/1-2, AOBJ: 610728 und RA 2100/1-2, AOBJ: 610729). Die digitale Edition von Theodor Fontanes Notizbüchern ist ein Projektergebnis; sie wird von Gabriele Radecke herausgegeben. Die Projektpartner sind die  [Theodor Fontane-Arbeitsstelle](http://www.uni-goettingen.de/de/154180.html) des Seminars für Deutsche Philologie an der Georg-August-Universität Göttingen und die [Niedersächsiche Staats- und Universitätsbibliothek](http://www.sub.uni-goettingen.de) (SUB Göttingen). Das Projekt nutzt Services von DARIAH-DE – Digital Research Infrastructure for the Arts and Humanities ([DARIAH-DE](http://www.de.dariah.eu)) und von TextGrid – Virtuelle Forschungsumgebung für die Geisteswissenschaften ([TextGrid](http://www.textgrid.de)).





### Anbieter

Anbieter dieser Internetpräsenz ist im Rechtssinne die Niedersächsische Staats- und Universitätsbibliothek Göttingen.




Georg-August-Universität Göttingen Niedersächsische Staats- und Universitätsbibliothek Göttingen




Platz der Göttinger Sieben 1




37073 Göttingen




Tel.: +49 (0)551 / 39-5212




Fax: +49 (0)551 / 39-5222




E-Mail: sekretariat(at)sub.uni-goettingen(dot)de





Die Niedersächsische Staats- und Universitätsbibliothek Göttingen wird vertreten durch den leitenden Direktor Dr. Wolfram Horstmann. Die SUB Göttingen ist als zentrale Einrichtung eine organisatorische, rechtlich nicht selbständige Einheit der Georg-August-Universität Göttingen. Die Georg-August-Universität Göttingen ist eine Körperschaft des öffentlichen Rechts. Sie wird durch die Präsidentin Prof. Dr. Ulrike Beisiegel gesetzlich vertreten.




Georg-August-Universität Göttingen




Wilhelmsplatz 1




37073 Göttingen




Tel.: +49 (0)551 / 39-0




Fax: +49 (0)551 / 39-9612




E-Mail: poststelle(at)uni-goettingen(dot)de





### Zuständige Aufsichtsbehörde

Georg-August-Universität Göttingen Stiftung Öffentlichen Rechts




Stiftungsausschuss Universität (§§ 59 Abs. 2, 60 Abs. 2 Satz 2 Nr. 7,60a Abs. 1 NHG)
Wilhelmsplatz 1




37073 Göttingen




Umsatzsteuer-Identifikationsnummer gemäß § 27 a Umsatzsteuergesetz: DE 152 336 201





### Inhaltlich Verantwortliche gemäß § 55 Abs. 2 RStV

#### Editions- und Literaturwissenschaft
Dr. Gabriele Radecke




Georg-August-Universität Göttingen




Seminar für Deutsche Philologie




Theodor Fontane-Arbeitsstelle




Käte-Hamburger-Weg 3




D – 37073 Göttingen




Tel.: +49 (0)551 / 39-10854




Fax: +49 (0)551 / 39-7511




E-Mail: gabriele.radecke(at)phil(dot)uni-goettingen(dot)de









und









#### Informationswissenschaft und Informationstechnologie

Dr. Mirjam Blümm




Niedersächsische Staats- und Universitätsbibliothek




Abteilung Forschung & Entwicklung






Papendiek 14




D – 37073 Göttingen




Tel.: +49 (0)551 / 39-9061




Fax: +49 (0)551 / 39-33856




E-Mail: bluemm(at)sub(dot)uni-goettingen(dot)de





### Haftungshinweis

Trotz sorgfältiger inhaltlicher Kontrolle übernehmen wir keine Haftung für die Inhalte externer Links. Für den Inhalt der verlinkten Seiten sind ausschließlich deren Betreiberinnen und Betreiber verantwortlich.





### Zitation

Theodor Fontane: Notizbücher. Digitale genetisch-kritische und kommentierte Edition. Hrsg. von Gabriele Radecke. https://fontane-nb.dariah.eu/index.html. Version 0.1 vom 7.12.2015, abgerufen am [Datum des Abrufs]


### Lizenz

Alle auf dieser Webseite präsentierten und im Rahmen des Projekts erarbeiteten Inhalte der Edition sind unter der Lizenz „CC-BY-NC-ND-4.0 international“ zugänglich. Die Digitalisierung wurde aus Projektmitteln der Deutschen Forschungsgemeinschaft sowie vom Walter de Gruyter-Verlag finanziert. Die Digitalisate stehen unter der Lizenz CC0 zur Verfügung [https://creativecommons.org/publicdomain/zero/1.0/](https///creativecommons.org/publicdomain/zero/1.0/). Die ODD wird auf [github.com](http://github.com/martin-delaiglesia/Fontane-Notizbuecher) unter LGPL 3.0 Lizenz zur Verfügung gestellt. Der Softwarequellcode ist verfügbar via [gitlab.gwdg.de](https://gitlab.gwdg.de/fontane-notizbuecher/) und dort unter der LGPL 3.0 Lizenz veröffentlicht.

### Bildnachweis

Das Bild auf der Startseite ist ein Ausschnitt aus: Theodor Fontane. Ölporträt von Carl Breitbach (1883). Quelle: http://www.textgridrep.de, hdl:11858/00-1734-0000-0002-B0C0-D
