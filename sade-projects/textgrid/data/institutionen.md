# INSTITUTIONEN

*  Georg-August-Universität Göttingen, Seminar für Deutsche Philologie, [Theodor Fontane-Arbeitsstelle](http://www.uni-goettingen.de/de/154180.html)





*  Georg-August-Universität Göttingen, [Niedersächsische Staats- und Universitätsbibliothek Göttingen](http://www.sub.uni-goettingen.de)

*  Digital Research Infrastructure for the Arts and Humanities ([DARIAH-DE](http://www.de.dariah.eu)) 





*  TextGrid – Virtuelle Forschungsumgebung für die Geisteswissenschaften ([TextGrid](http://www.textgrid.de))



 

### Assoziierte Partnerin und Eigentümerin der Notizbücher Theodor Fontanes

*  [Staatsbibliothek zu Berlin](http://staatsbibliothek-berlin.de/die-staatsbibliothek/abteilungen/handschriften/), Preußischer Kulturbesitz, Handschriftenabteilung 
