xquery version "3.1";

import module namespace fontaneTransfo="http://fontane-nb.dariah.eu/Transfo" at "/db/apps/SADE/modules/fontane/transform.xqm";

declare namespace digilib="digilib:digilib";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace svg="http://www.w3.org/2000/svg";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xlink="http://www.w3.org/1999/xlink";

declare default element namespace "http://www.tei-c.org/ns/1.0";

declare option output:method "json";
declare option output:media-type "application/json";

(: see: http://exist-db.org/exist/apps/doc/xquery.xml#serialization :)
(:declare option output:method "json";:)
(:declare option output:media-type "application/json";:)

(:declare option output:indent "yes";:)
(:declare option exist:serialize "indent=yes";:)

declare variable $tei := request:get-parameter("tei", "textgrid:2smvd.0");
declare variable $img := request:get-parameter("img", "textgrid:164h2.1");
declare variable $surfaces := collection('/db/sade-projects/textgrid/data/xml/data/')//tei:sourceDoc/tei:surface[ends-with(@facs, substring-before($img, "."))];

declare variable $xywh :=
map:merge(
    for $surface in $surfaces
    let $iiifUrlArr := fontaneTransfo:digilib($surface) =>  tokenize('/')
    let $pctArr := $iiifUrlArr[starts-with(., 'pct:')] => substring(5) => tokenize(',')
    let $wx := $pctArr[1] cast as xs:float
    let $wy := $pctArr[2] cast as xs:float
    let $wh := $pctArr[4] cast as xs:float
    let $ww := $pctArr[3] cast as xs:float

    let $ix := doc("/db/sade-projects/textgrid/data/xml/data/217qs.xml")//digilib:image[@uri=substring-before($img, ".")]

    let $ixw := $ix/@width cast as xs:integer
    let $ixh := $ix/@height cast as xs:integer

    let
        $x := ($wx div 100) * $ixw,
        $y := ($wy div 100) * $ixh,
        $w := ($ww div 100) * $ixw,
        $h := ($wh div 100) * $ixh
    return
        map:entry(string($surface/@n), ($x, $y, $w, $h))
);

declare variable $dpcm := 236.2205;

declare function local:dispatch($node as node()) as item()* {
let $sf := string($node/ancestor-or-self::tei:surface[parent::tei:sourceDoc]/@n)
let
    $x := $xywh($sf)[1],
    $y := $xywh($sf)[2],
    $w := $xywh($sf)[3],
    $h := $xywh($sf)[4]

return
typeswitch($node)

(:    case element(tei:zone):)
(:        return <div>{local:passthru($node)}</div>:)

    case element(tei:zone)
        return
            let $style := data($node/@style)
            let $ulx := if($node/@ulx) then number($node/@ulx) else 0
            let $zx := $x + floor( ( $ulx * $dpcm  )  )

            let $uly := if($node/@uly) then number($node/@uly) else 0
            let $zy := $y + floor( ( $uly * $dpcm  )  )

            let $zw := if( $node/@lrx ) then $w - $zx - number($node/@lrx) * $dpcm else $zx - $w
            let $zh := if( $node/@lry ) then $h - $zy - number($node/@lry) * $dpcm else $h - $zy

            return
                map{
                    "@id": "_:N" || util:uuid(),
                    "@type": "oa:Annotation",
                    "motivation": "sc:painting",
                    "resource": map{
                        "@id": "_:N" || util:uuid(),
                        "@type": "cnt:ContentAsText",
                        "format": "text/plain",
                        "chars": string-join($node//text()),
                        "language": "de"
                        },
                    "on": "https://textgridlab.org/1.0/iiif/manifests/"
                            || $tei || "/canvas/" || $img || ".json#xywh="
                            || $zx || "," || $zy || ",50,50"
                }
    case text()
        return replace($node, '"', "&quot;")
    default
        return local:passthru($node)

};

declare function local:passthru($nodes as node()*) as item()* {
    for $node in $nodes/node() return local:dispatch($node)
};


let $imgurl := "http://textgridrep.org/"||$img


let $xml :=
map {
    "@context": "http://www.shared-canvas.org/ns/context.json",
    "@id": "http://localhost:8080/exist/rest/apps/SADE/modules/fontane/anno.xq?tei="
            || $tei || "&amp;img=" || $img,
    "@type": "sc:AnnotationList",
    "resources": array {
        $surfaces//tei:zone ! local:dispatch(.)
    }
}

let $egal := response:set-header("Access-Control-Allow-Origin", "*")

return
    $xml
