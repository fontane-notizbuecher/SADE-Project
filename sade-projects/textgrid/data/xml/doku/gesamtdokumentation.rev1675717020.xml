<div>
    <xhtml:h2 xmlns:xhtml="http://www.w3.org/1999/xhtml">
        <div class="block-header">
            <h2>
                <span class="title">Editorische Gesamtdokumentation</span>
                <span class="decoration"/>
                <span class="decoration"/>
                <span class="decoration"/>
            </h2>
            <div id="toggleToc">
                <i class="fa fa-caret-down" aria-hidden="true"/>
            </div>
            <div id="dokuToc"/>
        </div>
    </xhtml:h2>
    <xhtml:div xmlns:xhtml="http://www.w3.org/1999/xhtml">

<xhtml:p>
von Gabriele Radecke unter Mitarbeit von Martin de la Iglesia und Matthias Göbel.
</xhtml:p>
<xhtml:div class="note">important: Beta-Version vom 15. Juli 2022
</xhtml:div>
<xhtml:p>
Die Gesamtdokumentation zur digitalen Fontane-Notizbuch-Edition fasst das Gesamtkonzept zusammen. Im Einzelnen findet man folgende Informationen:
</xhtml:p>
<xhtml:ul>
            <xhtml:li>
                <xhtml:div> Editionsprinzipien,</xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.1">Transkriptionsregeln</xhtml:a> und <xhtml:a href="?id=gesamtdokumentation_iii.2">Regeln für den Edierten Text</xhtml:a> und die <xhtml:a href="?id=gesamtdokumentation_iv._prinzipien_der_textkritischen_und_genetischen_apparate">textkritischen und genetischen Apparate</xhtml:a>,  </xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> Codierungsregeln in TEI/XML und Informationen über den <xhtml:a href="?id=gesamtdokumentation_ii._tei-header">TEI-Header</xhtml:a>, </xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> Konzepte der Visualisierung sowie</xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> Hinweise zur <xhtml:a href="?id=register">Register</xhtml:a>- und <xhtml:a href="?id=gesamtdokumentation_iii.3.26">Entitätencodierung</xhtml:a> sowie zur <xhtml:a href="?id=gesamtdokumentation_viii._stellenkommentar">Stellenkommentierung</xhtml:a> und zu den <xhtml:a href="?id=gesamtdokumentation_vii._ueberblickskommentar_zu_allen_notizbuechern">Überblickskommentaren</xhtml:a>.<xhtml:br/>
</xhtml:div>
</xhtml:li>
        </xhtml:ul>

<xhtml:p>
Die Gesamtdokumentation enthält darüber hinaus auch <xhtml:a href="?id=gesamtdokumentation_i._aufbau_des_online-portals">Hinweise zur Nutzung des Fontane-Notizbuch-Portals</xhtml:a>.<xhtml:br/>

</xhtml:p>

<xhtml:p>
Zur Veranschaulichung wurden prominente Beispiele ausgewählt und Scanausschnitte eingefügt.<xhtml:br/>

</xhtml:p>

<xhtml:p>
Die Gesamtdokumentation fungierte zunächst als internes Nachschlagewerk während der Projektarbeit. Darüber hinaus bietet sie nunmehr für die Benutzung der digitalen Edition eine transparente Übersicht zu allen editorischen Prinzipien, Richtlinien und Entscheidungen. Mit der Publikation der Gesamtdokumentation ist zudem ein weiterer Zweck beabsichtigt: Erstmals wurde Fontanes Arbeitsweise systematisch auf der Grundlage einer äußerst heterogenen und sehr umfangreichen Überlieferung ausgewertet und zusammengestellt. Fontanes Notizbücher eigneten sich dafür besonders, weil sie im Unterschied zu anderen Handschriftenkonvoluten viele unterschiedliche Textsorten enthalten, die auf verschiedenartige Weise entstanden sind. So findet man nicht nur Reinschriften, wie dies etwa bei Fontanes Briefhandschriften der Fall ist, sondern ähnlich wie bei seinen anderen Werkhandschriften auch Dispositionen, Entwürfe, Notizen, überarbeitete Niederschriften, <xhtml:a href="?id=gesamtdokumentation_iii.3.5">An- und Aufklebungen</xhtml:a> und zahlreiche <xhtml:a href="?id=gesamtdokumentation_iii.3.21#grossflaechige_ueberschreibung_palimpsest_ueberlappung">Textüberschichtungen</xhtml:a>. Auch die <xhtml:a href="?id=gesamtdokumentation_iii.3.21#schreibstoffe_und_-geraete">Schreibgeräte</xhtml:a> sind unterschiedlich sowie der <xhtml:a href="?id=gesamtdokumentation_iii.3.21#duktus">Duktus</xhtml:a>. Somit fungiert die folgende Dokumentation nicht nur als Grundlage für die digitale Notizbuch-Edition, sondern letztendlich auch als ein Kompendium für zukünftige digitale Fontane-Brief- und Werk-Editionen. Schließlich könnte die Gesamtdokumentation ebenso auch für Notizbuch-Editionen anderer Autoren eine Hilfestellung sein, weil sie über Fontanes individuelle Arbeitsweise hinaus die <xhtml:a href="?id=gesamtdokumentation_iii.3.1">Auswertung der material- und medienbedingten Notizbucheigenschaften</xhtml:a> zur Verfügung stellt. (Zum Begriff der Notizbuch-Edition vgl. <xhtml:a href="https://fontane-nb.dariah.eu/literaturvz.html?id=Radecke_2013" rel="ugc nofollow">Radecke 2013</xhtml:a>.)<xhtml:br/>

</xhtml:p>

<xhtml:p>
Schrifttypen:<xhtml:br/>

Im Portal werden die folgenden Schrifttypen verwendet:
</xhtml:p>
<xhtml:ul>
            <xhtml:li>
                <xhtml:div> Handschriften (Transkription/Edierter Text):<xhtml:br/>
</xhtml:div>
<xhtml:ul>
                    <xhtml:li>
                        <xhtml:div> Standardschrift Deutsche Schreibschrift (Kurrent): Linux Libertine</xhtml:div>
</xhtml:li>
                    <xhtml:li>
                        <xhtml:div> Lateinische Schrift: Free Sans</xhtml:div>
</xhtml:li>
                    <xhtml:li>
                        <xhtml:div> Archivalische Foliierung (je nach Verfügbarkeit): Lucida Console, Monaco oder Monospace</xhtml:div>
</xhtml:li>
                </xhtml:ul>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> Drucke (Transkription/Edierter Text):<xhtml:br/>
</xhtml:div>
<xhtml:ul>
                    <xhtml:li>
                        <xhtml:div> Fraktur: UnifrakturMaguntia</xhtml:div>
</xhtml:li>
                    <xhtml:li>
                        <xhtml:div> Antiqua: &lt;note important&gt;important: N.N.&lt;/note&gt;</xhtml:div>
</xhtml:li>
                </xhtml:ul>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> Außerhalb der Edition: Lato</xhtml:div>
</xhtml:li>
        </xhtml:ul>

</xhtml:div>
    <xhtml:h3 xmlns:xhtml="http://www.w3.org/1999/xhtml" id="inhaltsverzeichnis">
        <span class="dokuLink">
            <a href="#inhaltsverzeichnis">
                <i class="fa fa-link" aria-hidden="true"/>
            </a>
        </span>Inhaltsverzeichnis</xhtml:h3>
    <xhtml:div xmlns:xhtml="http://www.w3.org/1999/xhtml">
<xhtml:ul>
            <xhtml:li>
                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_i._aufbau_des_online-portals">I. Hinweise zur Benutzung des Fontane-Notizbuch-Portals</xhtml:a>
                </xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_ii._tei-header">II. TEI-Header</xhtml:a>
                </xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> III. Prinzipien der Transkription/Textkonstitution (Online-Edition)</xhtml:div>
<xhtml:ul>
                    <xhtml:li>
                        <xhtml:div> Die Festlegung der Prinzipien der Transkription/Textkonstitution sind dem Kompromiss von exakter und relativer Wiedergabe der Materialität der Notizbücher und der Notizbuchaufzeichnungen verpflichtet. </xhtml:div>
</xhtml:li>
                    <xhtml:li>
                        <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.1">1. Grundsätzliches zur Transkription</xhtml:a>
                        </xhtml:div>
</xhtml:li>
                    <xhtml:li>
                        <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.2">2. Grundsätzliches zur Textkonstitution (Edierter Text)</xhtml:a>
                        </xhtml:div>
</xhtml:li>
                    <xhtml:li>
                        <xhtml:div> 3. Zusammenführung der Prinzipien und Codierung in TEI/XML</xhtml:div>
<xhtml:ul>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.1">3.1 Äußere physische Beschaffenheit der Notizbücher</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.2">3.2 Texteingriffe</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.3">3.3 Einträge</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.4">3.4 Schreiberhände und Druckschriften</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.5">3.5 Aufgeklebte und angeklebte Blätter</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.6">3.6 Klebespuren und Fragmente aufgeklebter/angeklebter Blätter</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.7">3.7 Skizzen</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.8">3.8 Seiten- und Zeilenumbruch</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.9">3.9 Abstand zu den Blatträndern</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.10">3.10 Fettflecken</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.11">3.11 Abstände zwischen Buchstaben und Satzzeichen</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.12">3.12 Unbeschriftete Seiten (Vakat-Seiten)</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.13">3.13 Blattfragmente</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.14">3.14 Anfang und Ende einer Niederschrift</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.15">3.15 Unterbrochene Aufzeichnungen</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.16">3.16 Verlinkung von Notizbuchaufzeichnungen</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.17">3.17 Zeilenabstände</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.18">3.18 Sprachen</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.19">3.19 Schriftgröße</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.20">3.20 Beilagen</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.21">3.21 Handschriftliche Aufzeichnungen</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.22">3.22 Drucke</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.23">3.23 Zusammenführung der Prinzipien der Unterstreichungen (Edierter Text)</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.24">3.24 Zusammenführung der Prinzipien der Durchstreichungen (Edierter Text)</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.25">3.25 Zusammenführung der Prinzipien der Überschreibungen (Edierter Text)</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.26">3.26 Entitäten</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.27">3.27 Zusammenführung der genetischen Informationen</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                            <xhtml:li>
                                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iii.3.28">3.28 Unsicherere Befunde und Zweifelsfälle</xhtml:a>
                                </xhtml:div>
</xhtml:li>
                        </xhtml:ul>
</xhtml:li>
                </xhtml:ul>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_iv._prinzipien_der_textkritischen_und_genetischen_apparate">IV. Prinzipien der textkritischen und genetischen Apparate</xhtml:a>
                </xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_v._prinzipien_der_rekonstruktion_der_schreibchronologie">V. Prinzipien der Rekonstruktion der Schreibchronologie</xhtml:a> </xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_vi._buchedition">VI. Prinzipien der Buchedition</xhtml:a>
                </xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_vii._ueberblickskommentar_zu_allen_notizbuechern"> VII. Überblickskommentare</xhtml:a>
                </xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_viii._stellenkommentar"> VIII. Stellenkommentar</xhtml:a>
                </xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_ix._anmerkungen_online-edition_tooltip"> IX. Anmerkungen zur Online-Edition (tooltip/buttons/Randspalte)</xhtml:a>
                </xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_inhaltsverzeichnis_der_herausgeberin_zu_allen_notizbuechern">Inhaltsverzeichnis</xhtml:a>
                </xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> <xhtml:a href="?id=register">Register</xhtml:a>
                </xhtml:div>
<xhtml:ul>
                    <xhtml:li>
                        <xhtml:div> <xhtml:a href="?id=ereignisregister">Register der Ereignisse</xhtml:a>
                        </xhtml:div>
</xhtml:li>
                    <xhtml:li>
                        <xhtml:div> <xhtml:a href="?id=koerperschaftenregister">Register der Körperschaften und Institutionen</xhtml:a>
                        </xhtml:div>
</xhtml:li>
                    <xhtml:li>
                        <xhtml:div> <xhtml:a href="?id=ortsregister">Register der Orte, Sehenswürdigkeiten und Bauwerke</xhtml:a>
                        </xhtml:div>
</xhtml:li>
                    <xhtml:li>
                        <xhtml:div> <xhtml:a href="?id=personenregister">Personenregister</xhtml:a>
                        </xhtml:div>
</xhtml:li>
                    <xhtml:li>
                        <xhtml:div> <xhtml:a href="?id=werkregister">Register der musikalischen Werke, literarischen und anderen Texte, Werke von Bildenden Künstlern und Werke Fontanes</xhtml:a>
                        </xhtml:div>
</xhtml:li>
                </xhtml:ul>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_literatur-_und_siglenverzeichnis">Literatur- und Siglenverzeichnis</xhtml:a>
                </xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> <xhtml:a href="?id=verzeichnis_der_abkuerzungen">Verzeichnis der Abkürzungen</xhtml:a>
                </xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_verzeichnis_der_sonderzeichen">Verzeichnis der Sonderzeichen</xhtml:a>
                </xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> <xhtml:a href="?id=verzeichnis_editorischer_abkuerzungen">Verzeichnis editorischer Abkürzungen</xhtml:a>
                </xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> <xhtml:a href="?id=gesamtdokumentation_verzeichnis_editorischer_zeichen">Verzeichnis editorischer Zeichen</xhtml:a> </xhtml:div>
</xhtml:li>
        </xhtml:ul>

</xhtml:div>
</div>