<div>
    <xhtml:p xmlns:xhtml="http://www.w3.org/1999/xhtml">
[Dieser Abschnitt gehört zu Teil III der <xhtml:a href="?id=gesamtdokumentation">Gesamtdokumentation]</xhtml:a>.
</xhtml:p>
    <xhtml:h2 xmlns:xhtml="http://www.w3.org/1999/xhtml">
        <div class="block-header">
            <h2>
                <span class="title">1. Grundsätzliches zur Transkription</span>
                <span class="decoration"/>
                <span class="decoration"/>
                <span class="decoration"/>
            </h2>
            <div id="toggleToc">
                <i class="fa fa-caret-down" aria-hidden="true"/>
            </div>
            <div id="dokuToc"/>
        </div>
    </xhtml:h2>
    <xhtml:div xmlns:xhtml="http://www.w3.org/1999/xhtml">
<xhtml:div class="note">important: Beta-Version vom 6. Oktober 2016
</xhtml:div>
<xhtml:p>
Die diplomatische und alineare Transkription der digitalen Notizbuch-Edition erfasst Fontanes und Emilie Fontanes Notizbuchaufzeichnungen sowie Friedrich Fontanes Einträge und alle weiteren Beschriftungen von zeitgenössischen und postumen Schreiberhänden (Handschriften und Drucke) als „Dokument“. Sie ist als Orientierung für die Beschäftigung mit der Überlieferung konzipiert und erarbeitet worden, ist aber auch ohne die Notizbuch-Digitalisate zu benutzen. Die Befunde der topographischen Dimensionen der Handschriften und Drucke wurden in einer Mischung aus exakter und relativer Genauigkeit transkribiert, codiert und visualisiert. Die Deutungen der Befunde werden im <xhtml:a href="?id=gesamtdokumentation_iv._prinzipien_der_textkritischen_und_genetischen_apparate">Apparat</xhtml:a> diskutiert. Der textkritische und genetische Apparat wird in der Transkriptionsansicht interaktiv durch mouseover/tooltip angezeigt.<xhtml:br/>

</xhtml:p>

<xhtml:p>
Die diplomatische Transkription erfasst die Zeichen zeichen-, zeilen, seiten- und relativ positionsgetreu; sie folgt der physischen Reihenfolge der Notizbuchblätter. Da die Laufweite der Schrift nur bei hervorgehobenen Sperrungen durch Codierung und Visualisierung berücksichtigt wird, können die in der Oberflächenansicht dargestellten Zeichen nur relativ positioniert wiedergegeben werden. Deshalb wird empfohlen, Transkription und Digitalisate parallel zu benutzen. Auf Texteingriffe wird verzichtet.<xhtml:br/>

</xhtml:p>

<xhtml:p>
Die Differenzierung zwischen deutscher und lateinischer Schrift bzw. Fraktur und Antiqua in den Drucken wird beibehalten, ebenso die unterschiedlichen Schreibungen für „ß“ („ß“ oder „ſs“) und die Geminationen über dem „m“ und „n“ sowie alle anderen <xhtml:a href="?id=gesamtdokumentation_verzeichnis_der_sonderzeichen">Sonderzeichen</xhtml:a>. Falls sich Buchstaben hinsichtlich der Groß- und Kleinschreibung nicht eindeutig unterscheiden lassen (das gilt beispielsweise für „F“/„f“), so werden diese sinngemäß transkribiert. Die unterschiedlichen Längen der Trenn- und Bindestriche sowie der Gedankenstriche in den Handschriften werden in der Transkription nicht exakt abgemessen, sondern semantisch-normiert erfasst. So werden intendierte Trenn- und Bindestriche als Viertelgeviertstriche und Gedankenstriche als Halbgeviertstriche wiedergegeben. Die in den historischen Drucken üblichen Geviertstriche für Gedankenstriche werden in der Transkription als Geviertstriche reproduziert. Gleiches gilt  bei handschriftlich intendierten Geviertstrichen, etwa bei Rechnungen. Die unterschiedlichen Schriftgrößen werden dann relativ erfasst, wenn mit der vergrößerten oder verkleinerten Schrift ein semantischer Grund angenommen werden kann. Gedrängte und verkleinerte Zeichen am Blattrand werden infolgedessen nicht kleiner dargestellt, es sei denn, es handelt sich um Hinzufügungen, die grundsätzlich kleiner dargestellt werden.<xhtml:br/>

</xhtml:p>

<xhtml:p>
Die genetischen Befunde werden direkt in der Transkription visualisiert und ggf. über mouseover/tooltip aufgelöst. <xhtml:a href="?id=gesamtdokumentation_iii.3.21#unterstreichungen">Unterstreichungen</xhtml:a> werden zeichengetreu erfasst und hinsichtlich ihrer Form differenziert, ebenso die <xhtml:a href="?id=gesamtdokumentation_iii.3.21#durchstreichungen">Durchstreichungen</xhtml:a>, <xhtml:a href="?id=gesamtdokumentation_iii.3.21#ueberschreibungen">Überschreibungen</xhtml:a>, <xhtml:a href="?id=gesamtdokumentation_iii.3.21#umstellungen_transpositionen">Umstellungen</xhtml:a>, Ersetzungen,  <xhtml:a href="?id=gesamtdokumentation_iii.3.21#mehrfachformulierungen_alternativvarianten">Mehrfachformulierungen</xhtml:a> und <xhtml:a href="?id=gesamtdokumentation_iii.3.21#schreibabbrueche">Schreibabbrüche</xhtml:a>. Die  <xhtml:a href="?id=gesamtdokumentation_iii.3.21#einweisungszeichen">Einweisungszeichen</xhtml:a> werden hinsichtlich ihrer formalen Ausprägung codiert und visualisiert. <xhtml:a href="?id=gesamtdokumentation_iii.3.21#lineare_hinzufuegungen">Lineare</xhtml:a>, <xhtml:a href="?id=gesamtdokumentation_iii.3.21#interlineare_hinzufuegungen">interlineare</xhtml:a> und <xhtml:a href="?id=gesamtdokumentation_iii.3.21#marginale_hinzufuegungen">marginale Hinzufügungen</xhtml:a> sowie <xhtml:a href="?id=gesamtdokumentation_iii.3.21#hinzufuegungen_auf_der_gegenueberliegenden_notizbuchseite">Hinzufügungen auf gegenüberliegenden Notizbuchseiten</xhtml:a> werden in einer kleineren Schrifttype dargestellt.<xhtml:br/>

</xhtml:p>

<xhtml:p>
Darüber hinaus wurden folgende Regeln beschlossen:<xhtml:br/>

</xhtml:p>
<xhtml:ul>
            <xhtml:li>
                <xhtml:div> Scheinbar durch Federauftrag oder Verwischung verursachte zusammengeschriebene Wörter werden nach der auch damals korrekten Schreibung als getrennte Wörter transkribiert.</xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> Verrutschte, verzerrte oder heruntergezogene Punkte, die zunächst wie Kommata aussehen, werden als Punkte wiedergegeben.</xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> Verschleifungen werden mit einem Punkt versehen.<xhtml:br/>
</xhtml:div>
</xhtml:li>
        </xhtml:ul>

<xhtml:p>
Zu den Einzelprinzipien der Transkription vgl. die Kapitel <xhtml:a href="?id=gesamtdokumentation_iii.3.21#handschriftliche_aufzeichnungen">3.21 Handschriftliche Aufzeichnungen</xhtml:a> und <xhtml:a href="?id=gesamtdokumentation_iii.3.22#drucke">3.22 Drucke</xhtml:a> der Editorischen Gesamtdokumentation.
</xhtml:p>

</xhtml:div>
</div>