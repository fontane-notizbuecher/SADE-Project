<div>
    <xhtml:p xmlns:xhtml="http://www.w3.org/1999/xhtml">
[Dieser Abschnitt gehört zu Teil III der <xhtml:a href="?id=gesamtdokumentation">Gesamtdokumentation]</xhtml:a>.
</xhtml:p>
    <xhtml:h2 xmlns:xhtml="http://www.w3.org/1999/xhtml">
        <div class="block-header">
            <h2>
                <span class="title">3.9 Abstand zu den Blatträndern</span>
                <span class="decoration"/>
                <span class="decoration"/>
                <span class="decoration"/>
            </h2>
            <div id="toggleToc">
                <i class="fa fa-caret-down" aria-hidden="true"/>
            </div>
            <div id="dokuToc">
                <ul>
                    <li>
                        <a shape="rect" href="#abstand_zum_linken_blattrand">3.9.1 Abstand zum linken Blattrand</a>
                    </li>
                    <li>
                        <a shape="rect" href="#abstand_zum_rechten_blattrand">3.9.2 Abstand zum rechten Blattrand</a>
                    </li>
                    <li>
                        <a shape="rect" href="#abstand_zum_oberen_blattrand">3.9.3 Abstand zum oberen Blattrand</a>
                    </li>
                    <li>
                        <a shape="rect" href="#abstand_zum_unteren_blattrand">3.9.4 Abstand zum unteren Blattrand</a>
                    </li>
                    <li>
                        <a shape="rect" href="#ausrichtung_von_ueberschriften">3.9.5 Ausrichtung von Überschriften</a>
                    </li>
                </ul>
            </div>
        </div>
    </xhtml:h2>
    <xhtml:div xmlns:xhtml="http://www.w3.org/1999/xhtml">
<xhtml:div class="note">important: Beta-Version vom 6. Oktober 2016
</xhtml:div>
<xhtml:p>
Die Abstände zum linken Blattrand werden für die Transkription millimetergenau abgemessen. Für den Edierten Text wird der Abstand hingegen relativ wiedergeben. Es wird in vier Kategorien unterschieden, die bei Absätzen, Dialogzeilen, Listen, Überschriften und Versen vorkommen können:<xhtml:br/>

</xhtml:p>
<xhtml:ul>
            <xhtml:li>
                <xhtml:div> Stumpfer, linksbündiger Einzug (standard), der nicht codiert werden muss.</xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> Eingerückter Einzug (&lt;rend=„indent“&gt;)</xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> Zentrierter Einzug (jede einzelne Zeile soll zentriert dargestellt werden) (&lt;rend=„align(center)“&gt;)</xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> Rechtsbündiger Einzug (&lt;rend=„(align:right)“&gt;)</xhtml:div>
</xhtml:li>
        </xhtml:ul>

<xhtml:p>
Die Abstände zum oberen/unteren Blattrand werden nach Bedarf millimetergenau abgemessen.
</xhtml:p>
<xhtml:ul>
            <xhtml:li>
                <xhtml:div> Vgl. <xhtml:a href="?id=gesamtdokumentation_iii.3.8">3.8 Seiten- und Zeilenumbruch</xhtml:a>
                </xhtml:div>
</xhtml:li>
        </xhtml:ul>

</xhtml:div>
    <xhtml:h3 xmlns:xhtml="http://www.w3.org/1999/xhtml" id="abstand_zum_linken_blattrand">
        <span class="dokuLink">
            <a href="#abstand_zum_linken_blattrand">
                <i class="fa fa-link" aria-hidden="true"/>
            </a>
        </span>3.9.1 Abstand zum linken Blattrand</xhtml:h3>
    <xhtml:div xmlns:xhtml="http://www.w3.org/1999/xhtml">

<xhtml:p>
Für die Transkription werden die Abstände jeder Zeile millimetergenau zum linken Blattrand codiert. Ausnahme: Die archivalische Foliierung wird mit einem normierten Abstand zum linken Blattrand berücksichtigt. Hinzufügungen zwischen den Zeilen werden in ihrer Relation zum linken Blattrand positioniert und codiert; hier wird auch die Position im Verhältnis zur  Bezugszeile berücksichtigt. Für den Edierten Text werden nur semantisch relevante Abstände in normierter Form codiert und visualisiert (Einrückungen, Nicht-Einrückungen, Zentrierungen, Absätze stumpf/eingezogen); vgl. <xhtml:a href="?id=gesamtdokumentation_iii.3.8#harte_zeilen-_und_seitenumbrueche">3.8.2 harte Zeilen- und Seitenumbrüche</xhtml:a>.
</xhtml:p>
<xhtml:ul>
            <xhtml:li>
                <xhtml:div> Beispiel: Notizbuch C08, Blatt 3r (Scan C08_003.jpg)<xhtml:br/>
</xhtml:div>
</xhtml:li>
        </xhtml:ul>

<xhtml:p>
<xhtml:a href="?id=c08_004_3r.png">
                <xhtml:img alt="" width="300" class="imgLazy" data-original="public/doku/c08_004_3r.png" src="public/img/loader.svg"/>
            </xhtml:a>     <xhtml:a href="?id=bsp_c08_3r_overlay.jpg">
                <xhtml:img alt="" width="300" class="imgLazy" data-original="public/doku/bsp_c08_3r_overlay.jpg" src="public/img/loader.svg"/>
            </xhtml:a>
</xhtml:p>
<xhtml:ul>
            <xhtml:li>
                <xhtml:div> Transkription: <xhtml:em>&lt;Abstand 9.5&gt;</xhtml:em> <xhtml:em>&lt;Archivar 1&gt;</xhtml:em> 3 <xhtml:em>&lt;Fontane&gt;</xhtml:em> <xhtml:em>&lt;Abstand 1.1&gt;</xhtml:em> F<xhtml:em class="underline">reitag d. 2. Oktob</xhtml:em>er. <xhtml:em>&lt;Abstand 1.5&gt;</xhtml:em> München zu Fuß und zu <xhtml:em>&lt;Abstand 1.2&gt;</xhtml:em> Wagen durchſtreift. Baſi- <xhtml:em>&lt;Abstand 1.4&gt;</xhtml:em> lika, Ludwigskirche, Treppen- </xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> Edierter Text: <xhtml:em class="underline">Freitag d. 2. Oktober</xhtml:em>. <xhtml:em>&lt;Abstand normiert&gt;</xhtml:em> München zu Fuß und zu Wagen durchstreift. Basilika, Ludwigskirche, Treppen-</xhtml:div>
<xhtml:ul>
                    <xhtml:li>
                        <xhtml:div> TEI/XML:	Die Abstände werden in ihrer Relation zum linken Blattrand millimetergenau wiedergegeben (TEI: @style=„margin-left“).</xhtml:div>
</xhtml:li>
                </xhtml:ul>
</xhtml:li>
        </xhtml:ul>
<xhtml:pre class="code xml">
            <xhtml:span class="sc3">
                <xhtml:span class="re1">&lt;handShift</xhtml:span> <xhtml:span class="re0">new</xhtml:span>=<xhtml:span class="st0">"#Archivar1"</xhtml:span>
                <xhtml:span class="re2">/&gt;</xhtml:span>
            </xhtml:span>
<xhtml:span class="sc3">
                <xhtml:span class="re1">&lt;line</xhtml:span> <xhtml:span class="re0">style</xhtml:span>=<xhtml:span class="st0">"margin-left:9.5cm"</xhtml:span>
                <xhtml:span class="re2">&gt;</xhtml:span>
                <xhtml:span class="re1">&lt;fw</xhtml:span> <xhtml:span class="re0">type</xhtml:span>=<xhtml:span class="st0">"pageNum"</xhtml:span>
                <xhtml:span class="re2">&gt;</xhtml:span>
            </xhtml:span>3<xhtml:span class="sc3">
                <xhtml:span class="re1">&lt;/fw<xhtml:span class="re2">&gt;</xhtml:span>
                </xhtml:span>
                <xhtml:span class="re1">&lt;/line<xhtml:span class="re2">&gt;</xhtml:span>
                </xhtml:span>
            </xhtml:span>
<xhtml:span class="sc3">
                <xhtml:span class="re1">&lt;handShift</xhtml:span> <xhtml:span class="re0">new</xhtml:span>=<xhtml:span class="st0">"#Fontane"</xhtml:span>
                <xhtml:span class="re2">/&gt;</xhtml:span>
            </xhtml:span>
<xhtml:span class="sc3">
                <xhtml:span class="re1">&lt;line</xhtml:span> <xhtml:span class="re0">stlye</xhtml:span>=<xhtml:span class="st0">"margin-left:1.1cm"</xhtml:span>
                <xhtml:span class="re2">&gt;</xhtml:span>
                <xhtml:span class="re1">&lt;hi<xhtml:span class="re2">&gt;</xhtml:span>
                </xhtml:span>
            </xhtml:span>F<xhtml:span class="sc3">
                <xhtml:span class="re1">&lt;seg</xhtml:span> <xhtml:span class="re0">style</xhtml:span>=<xhtml:span class="st0">"text-decoration:underline"</xhtml:span>
                <xhtml:span class="re2">&gt;</xhtml:span>
            </xhtml:span>reitag d. 2. Oktob<xhtml:span class="sc3">
                <xhtml:span class="re1">&lt;/seg<xhtml:span class="re2">&gt;</xhtml:span>
                </xhtml:span>
            </xhtml:span>er.<xhtml:span class="sc3">
                <xhtml:span class="re1">&lt;/hi<xhtml:span class="re2">&gt;</xhtml:span>
                </xhtml:span>
                <xhtml:span class="re1">&lt;/line<xhtml:span class="re2">&gt;</xhtml:span>
                </xhtml:span>
            </xhtml:span>
<xhtml:span class="sc3">
                <xhtml:span class="re1">&lt;line</xhtml:span> <xhtml:span class="re0">style</xhtml:span>=<xhtml:span class="st0">"margin-left:1.5cm"</xhtml:span>
                <xhtml:span class="re2">&gt;</xhtml:span>
            </xhtml:span>München zu Fuß und zu<xhtml:span class="sc3">
                <xhtml:span class="re1">&lt;/line<xhtml:span class="re2">&gt;</xhtml:span>
                </xhtml:span>
            </xhtml:span>
<xhtml:span class="sc3">
                <xhtml:span class="re1">&lt;line</xhtml:span> <xhtml:span class="re0">stlye</xhtml:span>=<xhtml:span class="st0">"margin-left:1.2cm"</xhtml:span>
                <xhtml:span class="re2">&gt;</xhtml:span>
            </xhtml:span>Wagen durchſtreift. Baſi-<xhtml:span class="sc3">
                <xhtml:span class="re1">&lt;/line<xhtml:span class="re2">&gt;</xhtml:span>
                </xhtml:span>
            </xhtml:span>
<xhtml:span class="sc3">
                <xhtml:span class="re1">&lt;line</xhtml:span> <xhtml:span class="re0">style</xhtml:span>=<xhtml:span class="st0">"margin-left:1.4cm"</xhtml:span>
                <xhtml:span class="re2">&gt;</xhtml:span>
            </xhtml:span>lika, Ludwigskirche, Treppen-<xhtml:span class="sc3">
                <xhtml:span class="re1">&lt;/line<xhtml:span class="re2">&gt;</xhtml:span>
                </xhtml:span>
            </xhtml:span>
        </xhtml:pre>
<xhtml:ul>
            <xhtml:li>
                <xhtml:div> Visualisierung (Transkription): Alle Abstände werden wie codiert wiedergeben. <xhtml:a href="https://fontane-nb.dariah.eu/edition.html?id=%2Fxml%2Fdata%2F16bsn.xml&amp;page=3r" rel="nofollow">Beispiel</xhtml:a>
                </xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> Visualisierung (Edierter Text): Normierter Abstand zum linken Blattrand.</xhtml:div>
</xhtml:li>
        </xhtml:ul>

</xhtml:div>
    <xhtml:h3 xmlns:xhtml="http://www.w3.org/1999/xhtml" id="abstand_zum_rechten_blattrand">
        <span class="dokuLink">
            <a href="#abstand_zum_rechten_blattrand">
                <i class="fa fa-link" aria-hidden="true"/>
            </a>
        </span>3.9.2 Abstand zum rechten Blattrand</xhtml:h3>
    <xhtml:div xmlns:xhtml="http://www.w3.org/1999/xhtml">

<xhtml:p>
Die Abstände zum rechten Blattrand werden nicht wiedergegeben, da auf die Reproduktion der Buchstaben-Laufweite verzichtet wird.
</xhtml:p>

</xhtml:div>
    <xhtml:h3 xmlns:xhtml="http://www.w3.org/1999/xhtml" id="abstand_zum_oberen_blattrand">
        <span class="dokuLink">
            <a href="#abstand_zum_oberen_blattrand">
                <i class="fa fa-link" aria-hidden="true"/>
            </a>
        </span>3.9.3 Abstand zum oberen Blattrand</xhtml:h3>
    <xhtml:div xmlns:xhtml="http://www.w3.org/1999/xhtml">

<xhtml:p>
Der Abstand zum oberen Blattrand wird nur bei großen Abweichungen mit der Koordinate uly abgemessen.
</xhtml:p>
<xhtml:ul>
            <xhtml:li>
                <xhtml:div> Notizbuch C07, Blatt 1v (Scan C07_003.jpg)<xhtml:br/>
</xhtml:div>
</xhtml:li>
        </xhtml:ul>

<xhtml:p>
     <xhtml:a href="?id=bsp_c07_1v-2r.jpg">
                <xhtml:img alt="bsp_c07_1v-2r.jpg" width="750" title="bsp_c07_1v-2r.jpg" class="imgLazy" data-original="public/doku/bsp_c07_1v-2r.jpg" src="public/img/loader.svg"/>
            </xhtml:a>
</xhtml:p>
<xhtml:ul>
            <xhtml:li>
                <xhtml:div> Transkription: <xhtml:em>&lt;EZ F&gt;</xhtml:em> <xhtml:em>&lt;neue Zeile&gt;</xhtml:em> 1508 von Erfurt nach</xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> Edierter Text: 1508 von Erfurt nach</xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> TEI/XML: <xhtml:pre class="code xml">  <xhtml:span class="sc3">
                            <xhtml:span class="re1">&lt;zone</xhtml:span> <xhtml:span class="re0">uly</xhtml:span>=<xhtml:span class="st0">"1.8"</xhtml:span> <xhtml:span class="re0">lry</xhtml:span>=<xhtml:span class="st0">"7.2"</xhtml:span>
                            <xhtml:span class="re2">&gt;</xhtml:span>
                        </xhtml:span>
                <xhtml:span class="sc3">
                            <xhtml:span class="re1">&lt;handShift</xhtml:span> <xhtml:span class="re0">script</xhtml:span>=<xhtml:span class="st0">"clean"</xhtml:span>
                            <xhtml:span class="re2">/&gt;</xhtml:span>
                        </xhtml:span>
                <xhtml:span class="sc3">
                            <xhtml:span class="re1">&lt;line</xhtml:span> <xhtml:span class="re0">style</xhtml:span>=<xhtml:span class="st0">"margin-left:0.5cm"</xhtml:span>
                            <xhtml:span class="re2">&gt;</xhtml:span>
                            <xhtml:span class="re1">&lt;metamark</xhtml:span> <xhtml:span class="re0">function</xhtml:span>=<xhtml:span class="st0">"caret"</xhtml:span>
                            <xhtml:span class="re2">&gt;</xhtml:span>
                        </xhtml:span>╒<xhtml:span class="sc3">
                            <xhtml:span class="re1">&lt;/metamark<xhtml:span class="re2">&gt;</xhtml:span>
                            </xhtml:span>
                            <xhtml:span class="re1">&lt;/line<xhtml:span class="re2">&gt;</xhtml:span>
                            </xhtml:span>
                        </xhtml:span>
                <xhtml:span class="sc3">
                            <xhtml:span class="re1">&lt;line</xhtml:span> <xhtml:span class="re0">style</xhtml:span>=<xhtml:span class="st0">"margin-left:1.4cm"</xhtml:span>
                            <xhtml:span class="re2">&gt;</xhtml:span>
                        </xhtml:span>1508 von Erfurt nach<xhtml:span class="sc3">
                            <xhtml:span class="re1">&lt;/line<xhtml:span class="re2">&gt;</xhtml:span>
                            </xhtml:span>
                        </xhtml:span>
 </xhtml:pre>
</xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> Visualisierung (Transkription): Wie codiert. <xhtml:a href="https://fontane-nb.dariah.eu/edition.html?id=%2Fxml%2Fdata%2F16b00.xml&amp;page=1v" rel="nofollow">Beispiel</xhtml:a>
                </xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> Visualisierung (Edierter Text): Keine Berücksichtigung. </xhtml:div>
</xhtml:li>
        </xhtml:ul>

</xhtml:div>
    <xhtml:h3 xmlns:xhtml="http://www.w3.org/1999/xhtml" id="abstand_zum_unteren_blattrand">
        <span class="dokuLink">
            <a href="#abstand_zum_unteren_blattrand">
                <i class="fa fa-link" aria-hidden="true"/>
            </a>
        </span>3.9.4 Abstand zum unteren Blattrand</xhtml:h3>
    <xhtml:div xmlns:xhtml="http://www.w3.org/1999/xhtml">
<xhtml:ul>
            <xhtml:li>
                <xhtml:div> Beispiel: Notizbuch A14, Blatt 2v, (Scan A14_005.jpg) <xhtml:br/>
</xhtml:div>
</xhtml:li>
        </xhtml:ul>

<xhtml:p>
<xhtml:a href="?id=bsp_a14_2v.png">
                <xhtml:img alt="" width="300" class="imgLazy" data-original="public/doku/bsp_a14_2v.png" src="public/img/loader.svg"/>
            </xhtml:a>
</xhtml:p>
<xhtml:ul>
            <xhtml:li>
                <xhtml:div> Transkription: <xhtml:em>&lt;neue Zeile&gt;</xhtml:em> Die Apotheke da, wo <xhtml:em>&lt;lat. Schrift</xhtml:em> Pietzkers<xhtml:em>&gt;</xhtml:em> <xhtml:em>&lt;…&gt;</xhtml:em> <xhtml:em>&lt;neue Zeile&gt;</xhtml:em>Belvedere</xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> Edierter Text: Die Apotheke da, wo <xhtml:em>&lt;lat. Schrift</xhtml:em> Pietzkers<xhtml:em>&gt;</xhtml:em> &lt;…&gt; Belvedere</xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> TEI/XML <xhtml:pre class="code xml">
                        <xhtml:span class="sc3">
                            <xhtml:span class="re1">&lt;zone</xhtml:span> <xhtml:span class="re0">lry</xhtml:span>=<xhtml:span class="st0">"8.8"</xhtml:span>
                            <xhtml:span class="re2">&gt;</xhtml:span>
                        </xhtml:span>
            <xhtml:span class="sc3">
                            <xhtml:span class="re1">&lt;line</xhtml:span> <xhtml:span class="re0">style</xhtml:span>=<xhtml:span class="st0">"margin-left:1.5cm"</xhtml:span>
                            <xhtml:span class="re2">&gt;</xhtml:span>
                        </xhtml:span>Die Apotheke da, wo <xhtml:span class="sc3">
                            <xhtml:span class="re1">&lt;handShift</xhtml:span> <xhtml:span class="re0">script</xhtml:span>=<xhtml:span class="st0">"Latn"</xhtml:span>
                            <xhtml:span class="re2">/&gt;</xhtml:span>
                        </xhtml:span>Pietzkers<xhtml:span class="sc3">
                            <xhtml:span class="re1">&lt;/line<xhtml:span class="re2">&gt;</xhtml:span>
                            </xhtml:span>
                        </xhtml:span>
            <xhtml:span class="sc3">
                            <xhtml:span class="re1">&lt;handShift</xhtml:span> <xhtml:span class="re0">script</xhtml:span>=<xhtml:span class="st0">"Latf"</xhtml:span>
                            <xhtml:span class="re2">/&gt;</xhtml:span>
                        </xhtml:span>
            <xhtml:span class="sc3">&lt;...<xhtml:span class="re2">&gt;</xhtml:span>
                        </xhtml:span>
            <xhtml:span class="sc3">
                            <xhtml:span class="re1">&lt;line</xhtml:span> <xhtml:span class="re0">style</xhtml:span>=<xhtml:span class="st0">"margin-left:1.3cm"</xhtml:span>
                            <xhtml:span class="re2">&gt;</xhtml:span>
                        </xhtml:span>Belvedere<xhtml:span class="sc3">
                            <xhtml:span class="re1">&lt;/line<xhtml:span class="re2">&gt;</xhtml:span>
                            </xhtml:span>
                        </xhtml:span>
         <xhtml:span class="sc3">
                            <xhtml:span class="re1">&lt;/zone<xhtml:span class="re2">&gt;</xhtml:span>
                            </xhtml:span>
                        </xhtml:span>
                    </xhtml:pre>
</xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> Visualisierung (Transkription): Wie codiert. <xhtml:a href="https://fontane-nb.dariah.eu/edition.html?id=%2Fxml%2Fdata%2F2128k.xml&amp;page=2v" rel="nofollow">Beispiel</xhtml:a>
                </xhtml:div>
</xhtml:li>
            <xhtml:li>
                <xhtml:div> Visualisierung (Edierter Text): Keine Berücksichtigung.</xhtml:div>
</xhtml:li>
        </xhtml:ul>

</xhtml:div>
    <xhtml:h3 xmlns:xhtml="http://www.w3.org/1999/xhtml" id="ausrichtung_von_ueberschriften">
        <span class="dokuLink">
            <a href="#ausrichtung_von_ueberschriften">
                <i class="fa fa-link" aria-hidden="true"/>
            </a>
        </span>3.9.5 Ausrichtung von Überschriften</xhtml:h3>
    <xhtml:div xmlns:xhtml="http://www.w3.org/1999/xhtml">

<xhtml:p>
Vgl. <xhtml:a href="?id=gesamtdokumentation_iii.3.8#ueberschriften">3.8.2.3 Überschriften</xhtml:a>
</xhtml:p>

</xhtml:div>
</div>