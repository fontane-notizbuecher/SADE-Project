xquery version "3.1";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare option exist:serialize "method=text media-type=text/css omit-xml-declaration=yes";

(for $tei in collection('/db/sade-projects/textgrid/data/xml/data')/tei:TEI[tei:sourceDoc]
let $nb := substring-after($tei//tei:sourceDoc/@n, 'er_')
return
('#' || $nb || ' .sourceDoc > .surface {
    height:'||string($tei//tei:extent[1]/tei:dimensions[@type = 'leaf']/tei:height[1]/@quantity)||'mm;
    width:'||string($tei//tei:extent[1]/tei:dimensions[@type = 'leaf']/tei:width[1]/@quantity)||'mm;}
',
'#' || $nb || ' .sourceDoc > #sspine {
    width:'||string($tei//tei:extent[1]/tei:dimensions[@type = 'binding']/tei:depth[1]/@quantity)||'mm;}
',
'#' || $nb || ' .sourceDoc > #souter_back_cover, #'|| $nb || ' .sourceDoc > #souter_front_cover  {
    height:'||string($tei//tei:extent[1]/tei:dimensions[@type = 'binding']/tei:height[1]/@quantity)||'mm;
    max-height:'||string($tei//tei:extent[1]/tei:dimensions[@type = 'binding']/tei:height[1]/@quantity)||'mm !important;
    width:' ||string($tei//tei:extent[1]/tei:dimensions[@type = 'binding']/tei:width[1]/@quantity)||'mm;}
',
'#' || $nb || ' div.facs {
    height:'||string($tei//tei:extent[1]/tei:dimensions[@type = 'leaf']/tei:height[1]/@quantity)||'mm;
    width:'||string($tei//tei:extent[1]/tei:dimensions[@type = 'leaf']/tei:width[1]/@quantity)||'mm;}
',
'#' || $nb || ' .double {
    width:'||string(number($tei//tei:extent[1]/tei:dimensions[@type = 'leaf']/tei:width[1]/@quantity) * 2)||'.5mm;}
'
) (: return :)
) (: for :)
,
if (contains(request:get-cookie-value('facs'), 'inactive')) then '.facs {display:none;}' else (),
if (contains(request:get-cookie-value('trans'), 'inactive')) then '.surface {display:none;}' else (),
if (contains(request:get-cookie-value('xml'), 'inactive')) then '.teixml {display:none;}' else '.teixml {display:block;}'
