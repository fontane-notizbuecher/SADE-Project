xquery version "3.1";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare option exist:serialize "method=xml media-type=text/xml omit-xml-declaration=no indent=yes";

let $xpath := request:get-parameter("xpath", "date")
let $order := request:get-parameter("order", "unset")

let $ns := 'declare namespace tei="http://www.tei-c.org/ns/1.0";declare default element namespace "http://www.tei-c.org/ns/1.0";'

let $collection := "/db/sade-projects/textgrid/data/xml/data"
let $query := 'collection("' || $collection || '")//' || replace($xpath, "%23", "#")
let $fullquery := $ns || $query
let $results := util:eval($fullquery)

return
    <result hits="{count($results)}" xpath="{$xpath}" query="{$fullquery}">
        {
            switch ($order)
                case "notebook"
                    return
                        let $notebooks := distinct-values( $results ! ./base-uri() )
                        return
                            for $notebook in $notebooks
                            let $title := string(doc($notebook)//tei:titleStmt/tei:title)
                            let $subst := substring-after($title, " ")
                            let $substr := if( string-length($subst) gt 2 )
                                            then $subst
                                            else substring($subst, 1, 1) || "0" || substring($subst, 2, 1)
                            order by $substr
                            return
                                element notebook {
                                    attribute n {string( $title )},
                                    for $result in $results
                                    where $result/base-uri() = $notebook
                                    return $result
                                }
                default return $results
            }
    </result>
