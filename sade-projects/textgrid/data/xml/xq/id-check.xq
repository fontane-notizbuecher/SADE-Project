xquery version "3.1";

declare namespace tei="http://www.tei-c.org/ns/1.0";

let $relevant-ids := collection("/db/sade-projects/textgrid/data/xml/data/")//@xml:id[starts-with(parent::*/base-uri(), "/db/sade-projects/textgrid/data/xml/data/253")]
                        ! string(.)
let $observations := collection("/db/sade-projects/textgrid/data/xml/data/")//@ref
                        ! ( tokenize(., " ") ! tokenize(., ":")[last()] )

let $results := for $i in $relevant-ids
                    where count( index-of($observations, $i) ) = 0
                    return
                        $i
let $results := for $r at $pos in $results
                return
                    <result pos="{$pos}">
                        {$r, collection("/db/sade-projects/textgrid/data/xml/data/")//@xml:id[. = $r]/parent::* }
                    </result>
return
<results n="{count($results)}" count-ids="{count( $relevant-ids )}" count-obs="{ count($observations) }">{
    $results
}</results>
