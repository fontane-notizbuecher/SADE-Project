# Zitationshinweise

Theodor Fontane: Notizbücher. Digitale genetisch-kritische und kommentierte Edition. Hrsg. von Gabriele Radecke. https://fontane-nb.dariah.eu/index.html. Version 0.1 vom 7.12.2015, abgerufen am [Datum des Abrufs]





Theodor Fontane: Notizbuch C07 (Transkriptionsansicht). Hrsg. von Gabriele Radecke. In: Theodor Fontane: Notizbücher. Digitale genetisch-kritische und kommentierte Edition. Hrsg. von Gabriele Radecke, Blatt [Angabe der Blattrecto- und/oder der Blattverso-Seite]. https://fontane-nb.dariah.eu/index.html. Version 0.1 vom 7.12.2015, abgerufen am [Datum des Abrufs]








