<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:gnd="http://d-nb.info/standards/elementset/gnd#" exclude-result-prefixes="xs tei" version="2.0">
    <xsl:template match="/">
        <xsl:result-document href="godwin-simile-timeline.html" method="html">
            <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
                    <script src="http://api.simile-widgets.org/timeline/2.3.1/timeline-api.js?bundle=true" type="text/javascript"/>
                    <script>
                    function onLoad() {
                    var eventSource = new Timeline.DefaultEventSource();
                    var bandInfos = [
                    Timeline.createBandInfo({
                    eventSource:    eventSource,
                    date:           "1800",
                    width:          "95%", 
                    intervalUnit:   Timeline.DateTime.DECADE, 
                    intervalPixels: 30
                    }),
                    Timeline.createBandInfo({
                    overview:       true,
                    eventSource:    eventSource,
                    date:           "1800",
                    width:          "5%", 
                    intervalUnit:   Timeline.DateTime.CENTURY, 
                    intervalPixels: 60
                    })
                    ];
                    bandInfos[1].syncWith = 0;
                    bandInfos[1].highlight = true;
                    
                    tl = Timeline.create(document.getElementById("my-timeline"), bandInfos);
                    Timeline.loadXML("godwin-simile-timeline.xml", function(xml, url) { eventSource.loadXML(xml, url); });
                    }
                </script>
                </head>
                <body onload="onLoad();" onresize="onResize();">
                    <div id="my-timeline" style="height: 2500px; border: 1px solid #aaa"/>
                    <noscript>This page uses Javascript to show you a Timeline. Please enable Javascript in your browser to see the full page. Thank you.</noscript>
                </body>
            </html>
        </xsl:result-document>
        <xsl:result-document href="godwin-simile-timeline.xml" method="xml" indent="yes">
            <data>
                <xsl:apply-templates select="//tei:persName"/>
            </data>
        </xsl:result-document>
    </xsl:template>
    <xsl:template match="tei:persName">
        <xsl:if test="./@ref and not(contains(./@ref,'|'))">
            <xsl:variable name="path" select="substring-before(./@ref,'.')"/>
            <xsl:if test="document(concat('http://godwindiary.bodleian.ox.ac.uk',$path,'.xml'))//tei:birth/@when and (document(concat('http://godwindiary.bodleian.ox.ac.uk',$path,'.xml'))//tei:birth/@when ne '1000') and document(concat('http://godwindiary.bodleian.ox.ac.uk',$path,'.xml'))//tei:death/@when and (document(concat('http://godwindiary.bodleian.ox.ac.uk',$path,'.xml'))//tei:death/@when ne '1000') and not(preceding::tei:persName[@ref eq concat($path,'.html')])">
                <event>
                    <xsl:attribute name="durationEvent">true</xsl:attribute>
                    <xsl:attribute name="title">
                        <xsl:value-of select="concat(document(concat('http://godwindiary.bodleian.ox.ac.uk',$path,'.xml'))//tei:person/tei:persName/tei:forename,' ',document(concat('http://godwindiary.bodleian.ox.ac.uk',$path,'.xml'))//tei:person/tei:persName/tei:surname)"/>
                    </xsl:attribute>
                    <xsl:attribute name="start">
                        <xsl:value-of select="document(concat('http://godwindiary.bodleian.ox.ac.uk',$path,'.xml'))//tei:birth/@when"/>
                    </xsl:attribute>
                    <xsl:attribute name="end">
                        <xsl:value-of select="document(concat('http://godwindiary.bodleian.ox.ac.uk',$path,'.xml'))//tei:death/@when"/>
                    </xsl:attribute>
                </event>
            </xsl:if>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>