var options = {
    placeholder: "Suche…",
  	requestDelay: 333,
    url: function(query) {
        var arr = [];
        var checkvar = $('#index-query-selection input:checked');
        for (var i = 0, l = checkvar.length; i < l; i++ ) { arr.push(checkvar[i].name); };
    	return "api/index/search/" + query + "?index=" + arr.toString() ;
    	},
  	getValue: "name",
  	list: {
  		match: {
  			enabled: true
  		},
  		maxNumberOfElements: 15000
  	},
  	template: {
		type: "custom",
		method: function(value, item) {
		    switch (item.type) {
            case "person":
                var icon='<i class="fa fa-user" aria-hidden="true"/>';
            break;
            case "personGrp":
                var icon='<i class="fa fa-users" aria-hidden="true"/>';
            break;
            case "place":
                var icon='<i class="fa fa-map-marker" aria-hidden="true"/>';
            break;
            case "event":
                var icon='<i class="fa fa-flag" aria-hidden="true"/>';
            break;
            case "org":
                var icon='<i class="fa fa-university" aria-hidden="true"/>';
            break;
            case "item":
                var icon='<i class="fa fa-book" aria-hidden="true"/>';
            break;
            default:
                var icon='';
            }
            return '<a href="register.html?e=' + item.id + '">' + icon + ' ' + value + "</a>";
		}
	},
  	theme: "square"
  };
$("#index-query").easyAutocomplete(options);
