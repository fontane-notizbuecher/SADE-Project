// This JS mainly serves for displaying index entries in the edited text.

$(document).ready(function() {
    $('.options-item').click(function() {
        var indexType = $(this).attr("id");

        if($(this).hasClass('option-active')) {
            if(indexType == "persons") {
                $(".fa-user").addClass("icon-invisible");
                $(this).text("Zeige Personen");
            }
            else if(indexType == "place") {
                $(".fa-map-marker").addClass("icon-invisible");
                $(this).text("Zeige Orte");
            }
            else if(indexType == "event") {
                $(".fa-flag").addClass("icon-invisible");
                $(this).text("Zeige Ereignisse");
            }
            else if(indexType == "works") {
                $(".fa-book").addClass("icon-invisible");
                $(this).text("Zeige Werke");
            }
            else if(indexType == "orgs") {
                $(".fa-university").addClass("icon-invisible");
                $(this).text("Zeige Institutionen und Körperschaften");
            }

            $(this).removeClass('option-active');
        }
        else {
            if(indexType == "persons") {
                $(".fa-user").removeClass("icon-invisible");
                $(this).text("Blende Personen aus");
            }
            else if(indexType == "place") {
                $(".fa-map-marker").removeClass("icon-invisible");
                $(this).text("Blende Orte aus");
            }
            else if(indexType == "event") {
                $(".fa-flag").removeClass("icon-invisible");
                $(this).text("Blende Ereignisse aus");
            }
            else if(indexType == "works") {
                $(".fa-book").removeClass("icon-invisible");
                $(this).text("Blende Werke aus");
            }
            else if(indexType == "orgs") {
                $(".fa-university").removeClass("icon-invisible");
                $(this).text("Blende Institutionen und Körperschaften aus");
            }

            $(this).addClass("option-active");
        }
    });

    $('.clear-item').click(function() {
        $(".fa").addClass("icon-invisible");

        var actives = $('.option-active');
        $.each(actives, function(key, item) {
            var indexType = $(this).attr("id");

            if(indexType == "persons") {
                $(this).text("Zeige Personen");
            }
            else if(indexType == "place") {
                $(this).text("Zeige Orte");
            }
            else if(indexType == "event") {
                $(this).text("Zeige Ereignisse");
            }
            else if(indexType == "works") {
                 $(this).text("Zeige Werke");
            }
            else if(indexType == "orgs") {
                 $(this).text("Zeige Institutionen und Körperschaften");
            }

            $(this).removeClass('option-active');
        });
    });

    $("#infoViewBtn").click(function() {
        $(this).toggleClass("inactive");

        $('#helperUnderSectionHeader').toggleClass("helperUnderSectionHeader-active");
        $('#infoView').toggleClass("infoView-active");
    });
});
