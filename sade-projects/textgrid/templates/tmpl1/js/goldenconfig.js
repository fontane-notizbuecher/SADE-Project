var nb = getParameter("id").replace(/.xml|data|\//g, ""),
    page = getParameter("page"),
    code = {
                type: 'component',
                componentName: 'code',
                title:'Code',
                id: 'code',
                componentState: {
                    importclass: '.fcode'
                },
                width: 30,
                isClosable: false
          },
    codecontainer = $(".fcode").clone();
    tran = {
                type: 'component',
                componentName: 'tran',
                title:'Transkription',
                id: 'tran',
                componentState: {
                    importclass: '.ftran',
                    rotation: 0 },
                width: 35,
                isClosable: false
              },
    facs = {
                type: 'component',
                componentName: 'facs',
                title:'Faksimile',
                id: 'facs',
                componentState: {
                    importclass: '.ffacs',
                    rotation: 0},
                width: 35,
                isClosable: false
              },
    toc = {
         type: 'component',
                componentName: 'toc',
                title:'Inhaltsübersicht',
                id: 'toc',
                componentState: {
                    importclass: '.ftoc' },
                isClosable: false
    },
    comm = {
         type: 'component',
                componentName: 'comm',
                title:'Stellenkommentar (0)',
                id: 'comm',
                componentState: {
                    importclass: '.fcomm' },
                isClosable: false
    },
    index = {
         type: 'component',
                componentName: 'index',
                title:'Register (0)',
                id: 'index',
                componentState: {
                    importclass: '.findex' },
                isClosable: false
    },
    config = {
    settings: {showPopoutIcon: false, showCloseIcon: false},
    dimensions: {headerHeight: 30},
    content: [{
        type: 'row',
            content:[facs,tran,
            {
                type: "stack",
                content:
                    [code, toc, comm, index]

            }
            ]

    }]
};

// look for a saved state
var myLayout,
    savedState = localStorage.getItem( 'savedState' );

if( savedState !== null ) {
  	myLayout = new GoldenLayout( JSON.parse( savedState ), $('#goldenEdition')  );
} else {
  	myLayout = new GoldenLayout( config, $('#goldenEdition') );
}


// register components
myLayout.registerComponent( 'code', function( container, state ){
container.getElement().html( '<img alt="" class="codeLoader" src="https://fontane-nb.dariah.eu/public/img/loader.svg" width="500">');
    $.ajax({
        url: "get/code.html"+window.location.search,
        dataType: "text"
        }).done( function(data){
            $( ".codeLoader" ).replaceWith( data );
        });
});

myLayout.registerComponent( 'tran', function( container, state ){
    if ( getParameter("page").indexOf("-") > 0 && getParameter("page").indexOf("alt") == -1 ) {
        var newElement = '<div class="ftran sourceDoc double"><img alt="" class="facsLoader" src="https://fontane-nb.dariah.eu/public/img/loader.svg" width="500"></div>'
    }
    else {var newElement = '<div class="ftran sourceDoc"><img alt="" class="transLoader" src="https://fontane-nb.dariah.eu/public/img/loader.svg" width="500"></div>'}
container.getElement().html( newElement );
    $.ajax({
        url: "get/trans.html"+window.location.search,
        dataType: "text"
        }).done( function(data){
            var test = (getParameter("page").indexOf("-") > 0 && getParameter("page").indexOf("alt") == -1);
            $( ".ftran" ).append( data );
            registerInfo();
            if(!(test)) {outsider();}
            test4mod();
            highlightTarget();
            syncScroll( $(".ffacs").parent(), $(".ftran").parent() );
            if( myLayout.root.getItemsById("facs")[0] ) {
                rotate( myLayout.root.getItemsById("facs")[0].config.componentState.rotation );
            }
            highlightLinkedAreas();
            indexTranscriptionHighlighting();
            $( ".transLoader" ).remove();
        }).fail(function(e) {
            $(".ftran").html("<h3>error loading transcription</h3><p>If you want to get this error analysed, please send the following code and together with the current url to the support team.</p><div><code class='json hljs'>" + JSON.stringify(e) + "</code></div>");
        });
});

myLayout.registerComponent( 'facs', function( container, state ){
    if ( getParameter("page").indexOf("-") > 0 && getParameter("page").indexOf("alt") == -1 ) {
        var newElement = '<div class="ffacs double"><img alt="" class="facsLoader" src="https://fontane-nb.dariah.eu/public/img/loader.svg" width="500"></div>'
    }
    else {var newElement = '<div class="ffacs"><img alt="" class="facsLoader" src="https://fontane-nb.dariah.eu/public/img/loader.svg" width="500"></div>'}
container.getElement().html( newElement );
    $.ajax({
        url: "get/facs.html"+window.location.search,
        dataType: "text"
        }).done( function(data){
            $( ".ffacs" ).append( data );
            $("img.facs").on("error", function(e){
                $(".ffacs").html("<h3>error loading the image</h3><p>If you want to get this error analysed, please send the following code and together with the current url to the support team.</p><div><code class='json hljs'>" + JSON.stringify(e.currentTarget.currentSrc) + "\
                " + JSON.stringify(e)
                + "</code></div>");
            });
            syncScroll( $(".ffacs").parent(), $(".ftran").parent() );
            if( myLayout.root.getItemsById("facs")[0] ) {
                rotate( myLayout.root.getItemsById("facs")[0].config.componentState.rotation );
            }
            altFacs();
            document.getElementsByClassName("facs")[1].onload = function(){
                $( ".facsLoader" ).remove();
            };
        })
        .fail(function(e) {
            $(".ffacs").html("<h3>error loading facsimile</h3><p>If you want to get this error analysed, please send the following code and together with the current url to the support team.</p><div><code class='json hljs'>" + JSON.stringify(e) + "</code></div>");
        });
});

myLayout.registerComponent( 'toc', function( container, state ){
container.getElement().html( '<div class="ftoc"></div>');
    $.ajax({
        url: "api/toc/"+nb,
        dataType: "text"
        }).done( function(data){
            $( ".ftoc" ).append( data );
            $('li.target' + page).parent().parent().parent().addClass("in");
        })
        .fail(function(e) {
            $(".ftoc").html("<h3>error loading table of contents</h3><p>If you want to get this error analysed, please send the following code and together with the current url to the support team.</p><div><code class='json hljs'>" + JSON.stringify(e) + "</code></div>");
        });
});

myLayout.registerComponent( 'comm', function( container, state ){
container.getElement().html( '<div class="fcomm"></div>');
    $.ajax({
        url: "api/stk/" + nb + "/" + page,
        dataType: "text"
        }).done( function(data){
            $( ".fcomm" ).append( data );
            var currentTitle = container._config.title;
            var numComments = $(".fcomm").find(".editorialNote").length;
            var newTitle = currentTitle.replace(/\d+/g, numComments);
            container.setTitle(newTitle);
            $("li[data-ref]").each(function(){
                var target = $(this).attr("data-ref");
                $(this).hover(
                    function(){
                    highlight(target);
                    },
                    function(){
                      $(".fhighlighted").removeClass("fhighlighted");
                    });
            });
        })
        .fail(function(e) {
            $(".fcomm").html("<h3>error loading commentary</h3><p>If you want to get this error analysed, please send the following code and together with the current url to the support team.</p><div><code class='json hljs'>" + JSON.stringify(e) + "</code></div>");
        });
});

myLayout.registerComponent( 'index', function( container, state ){
container.getElement().html( '<div class="findex"></div>');
    $.ajax({
        url: "api/index/" + nb + "/"+page,
        dataType: "html"
        }).done( function(data){
            $( ".findex" ).append( data );
            var currentTitle = container._config.title;
            var numEntities = $(".findex").find("*[data-ref]").length;
            var newTitle = currentTitle.replace(/\d+/g, numEntities);
            container.setTitle(newTitle);
            indexTranscriptionHighlighting();
        })
        .fail(function(e) {
            $(".findex").html("<h3>error loading index data</h3><p>If you want to get this error analysed, please send the following code and together with the current url to the support team.</p><div><code class='json hljs'>" + JSON.stringify(e) + "</code></div>");
        });
});

myLayout.init();

if( localStorage.savedState ) {
    if( localStorage.savedState.indexOf("code") === -1) {
        $("#xmlBtn").toggleClass( "inactive" );
    }
    if( localStorage.savedState.indexOf("tran") === -1 ) {
        $("#transBtn").toggleClass( "inactive" );
    }
    if( localStorage.savedState.indexOf("facs") === -1 ) {
        $("#facsBtn").toggleClass( "inactive" );
    }
    if( localStorage.savedState.indexOf("toc") === -1 ) {
        $("#tocBtn").toggleClass( "inactive" );
    }
}

// save the state
myLayout.on( 'stateChanged', function(){
    var state = JSON.stringify( myLayout.toConfig() );
    localStorage.setItem( 'savedState', state );
});


$( window ).resize( function(){ myLayout.updateSize(); } );

function goldenActivate( trigger ){
    var child;
    if(trigger == 'code') { child = code; }
    else if(trigger == 'tran') { child = tran; }
    else if(trigger == 'toc') { child = toc; }
    else if(trigger == 'facs') { child = facs; };

    myLayout.root.contentItems[ 0 ].addChild(
        child
        );
    var insert;
    switch ( trigger ) {
        case 'code':
            $.ajax({
                url: "get/code.html"+window.location.search
                })
                .done( function(data){
                    myLayout.root.contentItems[0].getItemsById( trigger )[0].container.getElement().html( data );
                } );
            break;
        case 'tran':
            $.ajax({
                url: "get/trans.html"+window.location.search
                })
                .done( function(data){
                    if ( test ) {
                        var newElement = '<div class="ftran sourceDoc double">'
                    }
                    else {var newElement = '<div class="ftran sourceDoc">'}
                        myLayout.root.contentItems[0].getItemsById( trigger )[0].container.getElement()
                            .html(
                                newElement
                                + data + "</div>" );
                        syncScroll( $(".ffacs").parent(), $(".ftran").parent() );
                        registerInfo();
                        if(!(test)) {outsider();}
                        test4mod();
                        rotate( myLayout.root.getItemsById("facs")[0].config.componentState.rotation );
                        highlightLinkedAreas();
                        indexTranscriptionHighlighting();
                } );
            break;
        case 'facs':
            $.ajax({
                url: "get/facs.html"+window.location.search
                })
                .done( function(data){
                    var test = (getParameter("page").indexOf("-") > 0 && getParameter("page").indexOf("alt") == -1);
                    if ( test ) {
                        var newElement = '<div class="ffacs double">'
                    }
                    else {var newElement = '<div class="ffacs">'}
                        myLayout.root.contentItems[0].getItemsById( trigger )[0].container.getElement()
                            .html(
                                newElement
                                + data + "</div>" );
                        syncScroll( $(".ftran").parent(), $(".ffacs").parent() );
                        rotate( myLayout.root.getItemsById("facs")[0].config.componentState.rotation );
                        altFacs();
                } );
            break;
        default:
            console.log( "unknown trigger" );
    };
}

function goldenDestroy ( id ) {
    myLayout.root.getItemsById( id )[0].remove();
}

// toggle button
function tbtn(e){
    switch ( e ) {
            case 'code':
                $('#xmlBtn').addClass("inactive");
                break;
            default:
                console.log( "unprepared item toggle" );
        }
}


// stolen from goozbox @stackoverflow
function syncScroll(el1, el2) {
  var $el1 = $(el1);
  var $el2 = $(el2);

  // Lets us know when a scroll is organic
  // or forced from the synced element.
  var forcedScroll = false;

  // Catch our elements' scroll events and
  // syncronize the related element.
  $el1.scroll(function() { performScroll($el1, $el2); });
  $el2.scroll(function() { performScroll($el2, $el1); });

  // Perform the scroll of the synced element
  // based on the scrolled element.
  function performScroll($scrolled, $toScroll) {
    if (forcedScroll) return (forcedScroll = false);
    var percentTop = ($scrolled.scrollTop() /
        ($scrolled[0].scrollHeight - $scrolled.outerHeight())) * 100;
    var percentLeft = ($scrolled.scrollLeft() /
        ($scrolled[0].scrollWidth - $scrolled.outerWidth())) * 100;
    setScrollTopFromPercent($toScroll, percentTop);
    setScrollLeftFromPercent($toScroll, percentLeft);
  }

  // Scroll to a position in the given
  // element based on a percent.
  function setScrollTopFromPercent($el, percent) {
    var scrollTopPos = (percent / 100) *
      ($el[0].scrollHeight - $el.outerHeight());
    forcedScroll = true;
    $el.scrollTop(scrollTopPos);
  }
  function setScrollLeftFromPercent($el, percent) {
    var scrollLeftPos = (percent / 100) *
      ($el[0].scrollWidth - $el.outerWidth());
    forcedScroll = true;
    $el.scrollLeft(scrollLeftPos);
  }
}

// use the nb title to clear local storage
$("#nb-title").click(function(){
    localStorage.removeItem("savedState");
    location.reload();
});

// highlight the target element
function highlightTarget(){
  var t = getParameter("target");
  if (t != null){
    $('#'+ t ).addClass("fhighlighted");
    $("*[data-ref*='" + t + "'").each(
        function(){
            var ref = $(this).attr("data-ref");
            console.log(ref)
            var regex = new RegExp("(eve|wrk|org|psn|plc)"+ t + "$|(eve|wrk|org|psn|plc)" + t + " ", 'g');
            if( ref.match( regex ) )
                {$(this).addClass('fhighlighted');}
        } );
  }
}

// look for outsiders
function outsider() {
if ( $( ".surface" ).length ) {
        $( ".surface" ).each(function(){
        // check for an addition outside of the surface
        var minOffset = $( this ).offset().left;
        $('*[style*="left:-"]').each(function () {
          var thisOffset = $(this).offset().left;
          var newVal = minOffset - thisOffset;
          if ( thisOffset < minOffset ) {
            $(this).css('margin-left', newVal + 'px');
          }
        }
        );
        var minOffset = $( this ).offset().top;
        $('*[style*="left:-"]').each(function () {
          var thisOffset = $(this).offset().top;
          var newVal = minOffset - thisOffset;
          if (thisOffset < minOffset) {
            $(this).css('margin-left', newVal + 'px');
          }
        }
        );
        });

        if($(".addWrapper svg")[0]) {
        if( $('.addWrapper svg').closest('.surface').offset().left  >  $('.addWrapper svg').offset().left)
            {   var diff=$('.addWrapper svg').closest('.surface').offset().left - $('.addWrapper svg').offset().left;
                $('.addWrapper svg').css('margin-left', diff+'px' );
            }
        }

    }
}

function test4mod(){
    var i = 0;
    var uniq = new Set();
    for ( c of $("[class*='mod-seq']") ) {
        for ( k of c.className.split(" ") ) {
            if( k.startsWith("mod-seq") ) { uniq.add(k) }
        }
    }
    if (uniq.size === 0) {
        document.getElementById("seqBtn").style.display = "none";
    }
    var mod = uniq.size + 1;
    $("#seqBtn").click( function(){
        var posIcon = "1011" + (2 + i);
        i++;
        $("[class*='mod-seq']").addClass("hidden");
        $("[class*='mod-seq-" + i + "']").removeClass("hidden");
        document.getElementById("seqDisp").innerHTML = "" + "&#" + posIcon + ";";
        if(i === mod) {
            $("[class*='mod-seq']").removeClass("hidden");
            i = 0;
            document.getElementById("seqDisp").innerHTML = "";
        }
    } );
}

var $rotation = 0;
function rotateIt(deg){
    $rotation = $rotation + deg;
    $(".ffacs").css("transform", "rotate("+$rotation+"deg)");
}
myLayout.on("initialised", function(){
    if( myLayout.root.getItemsById("facs")[0] ) {
        rotate( myLayout.root.getItemsById("facs")[0].config.componentState.rotation );
    }
    // TODO: else?
});

function highlightLinkedAreas() {
  $(".surface *[id]").hover(function(){
      var id = $(this).attr("id");
      if ( $("*[data-ref~='"+id+"']").addClass("fhighlighted").length != 0 ) {
        $(this).addClass("fhighlighted");
      }
    }, function(){
      var id = $(this).attr("id");
      $(this).removeClass("fhighlighted");
      $("*[data-ref~='"+id+"']").removeClass("fhighlighted");
  });

  $(".surface *[data-ref]").hover(function(){
      var ids = $(this).attr("data-ref").split(" ");
      for (id of ids) {
        if ( $(".surface *[id='"+id+"']").addClass("fhighlighted").length != 0 ) {
          $(this).addClass("fhighlighted");
        }
      };
    }, function(){
      var ids = $(this).attr("data-ref").split(" ");
      for (id of ids) {
        $(this).removeClass("fhighlighted");
        $(".surface *[id='"+id+"']").removeClass("fhighlighted");
      };
  });
}

// getParameter function available everywhere
// implementation for LitVZ
function getParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
            .split("&")
                .forEach(function (item) {
                    tmp = item.split("=");
                    if (tmp[0] === parameterName) {
                      if (decodeURIComponent(tmp[1]) == "" && parameterName === "page") { result = "outer_front_cover" }
                      else { result = decodeURIComponent(tmp[1]); }
                    }
                });
    return result;
}

function indexTranscriptionHighlighting() {
    $(".findex li[data-ref]").each(function(){
        var target = $(this).attr("data-ref");
        $(this).hover(
            function(){
            highlight(target);
            },
            function(){
              $(".fhighlighted").removeClass("fhighlighted");
            });
    });
}

function altFacs() {
    $(".alt-facs > a").click(function (e) {
        var src = $(e.target).attr("data-src");
        var miradorHref=$(e.target).attr("data-href");
        var div = $( e.target).closest("div.facs")[0];
        var oldSrc = $(div).find("img").attr("src");
        var oldHref = $(div).children("a").attr("href");
        $(div).find("img").attr("src", src);
        $(div).children("a").attr("href", miradorHref);
        $(e.target).attr("data-src", oldSrc);
        $(e.target).attr("data-href", oldHref);
    });
}
