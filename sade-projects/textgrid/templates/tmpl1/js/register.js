// script for dynamic content in index view.
var getUrlParameter = function getUrlParameter(sParam) {
var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&amp;'),
    sParameterName,
    i;

for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : sParameterName[1];
    }
}
};

function click () {
    $(".load-entity").click(function(e){
            var load = e.currentTarget.dataset.load;
            $( e.target ).addClass("fa-spin");
            $.ajax({
                url : "api/index/entity/" + load
            }).done(function(data) {
                $( e.target ).parent().parent().after( data );
                $( e.target ).parent().parent().remove();});
    });
}

$( document ).ready(function() {
    var entity=getUrlParameter('e');
    var pos=$("#"+entity).offset().top - 150;
    $("html").animate({ scrollTop: pos }, 1500);

    $(".load-entity").click(function(e){
                var load = e.currentTarget.dataset.load;
                $( e.target ).addClass("fa-spin");
                $.ajax({
                    url : "api/index/entity/" + load
                }).done(function(data) {
                    $( e.target ).parent().parent().after( data );
                    $( e.target ).parent().parent().remove();
                    click();
                });
        });

    //load image from TextGrid Repository
    switch($(".registerEintrag")[0].classList[1]) {
                  case "place": var type = "plc"; break;
                  case "item": var type = "wrk"; break;
                  case "person": var type = "psn"; break;
                  case "personGrp": var type = "psn"; break;
                  case "event": var type = "eve"; break;
                  case "org": var type = "org"; break;
                  default:
                    console.log("unable to parse entity type. maybe the developer has to extend the list.");
    }

    fetch("api/index/image/" + type + "/" + entity)
    .then(response => response.json())
    .then(function(data) {
        var hits = data.length;
        if (hits > 0) {
          // link
            var link = document.createElement("a");
            link.setAttribute('href', 'https://textgridrep.org/' + data[0]);
            link.setAttribute('style', 'height: inherit;');
            link.setAttribute('target', '_blank');
          // imgage
            var img = new Image();
            link.appendChild(img);
            var div = document.getElementById('regImage');
            img.onload = function() {
              div.insertBefore(link, div.firstChild);
            };
            img.src = 'https://textgridlab.org/1.0/digilib/rest/IIIF/' + data[0] + '/full/,1000/0/native.jpg';
            img.style="display: block; height: inherit;";
        }
    });
});
