<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:rpa="rpa:rpa" xmlns:svg="http://www.w3.org/2000/svg" xmlns:digilib="digilib:digilib" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:sid="sid" version="2.0" xpath-default-namespace="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="tei">
    <xsl:strip-space elements="*"/><!-- parameters -->
    <xsl:param name="sid"/>
    <xsl:param name="rownum"/><!-- global variables -->
    <xsl:variable name="currentNotebook" select="substring-before(/*/*/surface[1]/@facs, '_')"/>
    <xsl:variable name="surfaceHeight" select="/TEI/teiHeader[1]/fileDesc[1]/sourceDesc[1]/msDesc[1]/physDesc[1]/objectDesc[1]/extent[1]/dimensions[@type='leaves']/height[1]/@quantity"/>
    <xsl:variable name="surfaceWidth" select="/TEI/teiHeader[1]/fileDesc[1]/sourceDesc[1]/msDesc[1]/physDesc[1]/objectDesc[1]/extent[1]/dimensions[@type='leaves']/width[1]/@quantity"/>
    <xsl:variable name="bindingHeight" select="/TEI/teiHeader[1]/fileDesc[1]/sourceDesc[1]/msDesc[1]/physDesc[1]/objectDesc[1]/extent[1]/dimensions[@type='binding']/height[1]/@quantity"/>
    <xsl:variable name="bindingWidth" select="/TEI/teiHeader[1]/fileDesc[1]/sourceDesc[1]/msDesc[1]/physDesc[1]/objectDesc[1]/extent[1]/dimensions[@type='binding']/width[1]/@quantity"/>
    <xsl:variable name="unit" select="/TEI/teiHeader[1]/fileDesc[1]/sourceDesc[1]/msDesc[1]/physDesc[1]/objectDesc[1]/extent[1]/dimensions[@type='leaves']/width[1]/@unit"/>
    <xsl:variable name="sessionId" select="'Snsz4I3pqPkJFs49zZJ3MCwFeVrlqVOWx0Zk7pwvqQ0Sq7AFy9U8aetxlR2wOJ7nBSPkiv5esB60tK11426507452988920'"/><!-- /global variables --><!-- STEP 1 -->
    <xsl:template match="/">
        <xsl:apply-templates select="$step1" mode="step2"/>
    </xsl:template>
    <xsl:variable name="step1">
        <xsl:call-template name="one"/>
    </xsl:variable>
    <xsl:template match="*" mode="step1">
        <xsl:copy>
            <xsl:sequence select="@*"/>
            <xsl:apply-templates mode="step1"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="text()" mode="step1"><!--<xsl:sequence select="preceding::tei:handShift[1]/@*"/><xsl:value-of select="."/>-->
        <xsl:if test="preceding::tei:handShift[@new]/@new = '#Fontane'">
            <xsl:variable name="medium" select="preceding::handShift[@medium][1]/@medium"/>
            <xsl:variable name="script">
                <xsl:choose>
                    <xsl:when test="contains(preceding::handShift[@script][1]/@script, ' ')">
                        <xsl:value-of select="preceding::handShift[@script][contains(@script, ' ')][1]/@script"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat(preceding::handShift[@script][contains(@script, 'standard') or contains(@script, 'clean') or contains(@script, 'hasty')][1]/@script, ' ',preceding::handShift[@script][contains(@script, 'Latn') or contains(@script, 'Latf')][1]/@script)"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="new" select="preceding::handShift[@new][1]/@new"/>
            <xsl:variable name="rendition" select="preceding::handShift[@rendition][1]/@rendition"/>
            <xsl:element name="tei:handShift">
                <xsl:attribute name="medium" select="$medium"/>
                <xsl:attribute name="script" select="$script"/>
                <xsl:attribute name="new" select="$new"/>
                <xsl:attribute name="rendition" select="$rendition"/>
                <xsl:value-of select="."/>
            </xsl:element>
        </xsl:if>
    </xsl:template>
    <xsl:template name="one">
        <xsl:apply-templates select="tei:TEI" mode="step1"/>
    </xsl:template><!-- /STEP 1 --><!-- HTML Framework -->
    <xsl:template match="/" mode="step2">
        <div id="ediTex" class="row">
            <xsl:apply-templates select="./tei:TEI/tei:sourceDoc/tei:surface"/>
        </div>
    </xsl:template><!-- /HTML Framework -->
    <xsl:template match="handShift">
        <xsl:if test="@new = '#Fontane'">
            <span>
                <xsl:attribute name="class">
                    <xsl:value-of select="normalize-space(concat(substring-after(@new, '#'), ' ',@medium, ' ', @script, ' ',substring-after(@rendition, '#')))"/>
                </xsl:attribute>
                <xsl:attribute name="title">
                    <xsl:value-of select="substring-after(@new, '#')"/>
                </xsl:attribute>
                <xsl:if test="@new != preceding::handShift[@new][1]/@new and not(contains(@new, '#Fontane') or contains(@new, '#Archivar'))">
                    <xsl:element name="img">
                        <xsl:attribute name="src">http://fontane-nb.dariah.eu:8080/js/pics/feder.png</xsl:attribute>
                        <xsl:attribute name="height">25px</xsl:attribute>
                        <xsl:attribute name="style">position:absolute; margin-left:-15px;</xsl:attribute>
                        <xsl:attribute name="alt">
                            <xsl:value-of select="substring-after(@new, '#')"/>
                        </xsl:attribute>
                        <xsl:attribute name="class">
                            <xsl:value-of select="concat('feder_',substring-after(@new, '#'))"/>
                        </xsl:attribute>
                    </xsl:element>
                </xsl:if>
                <xsl:apply-templates/>
            </span>
        </xsl:if>
    </xsl:template>
    <xsl:template match="tei:TEI/tei:sourceDoc/tei:surface">
        <xsl:variable name="maxuly">
            <xsl:choose>
                <xsl:when test="descendant::zone[not(@rotate)]/@uly = ''">0</xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="max(descendant::zone[not(@rotate)]/@uly)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="maxulylines" select="count(descendant::zone[@uly = $maxuly]/descendant::line)"/>
        <xsl:if test="contains(@n, 'outer')">
            <xsl:choose>
                <xsl:when test="contains(@type, 'empty')"><!--                    <div class="ruler hidden" title="⇿">--><!--                        <img style="float: right; max-height: 3cm; position:absolute; margin-left: 0; margin-top: 10px;" src="http://fontane-nb.dariah.eu:8080/js/pics/ruler-17cm.png" alt="Ruler 17cm"/>--><!--                    </div>--></xsl:when>
                <xsl:otherwise><!--                    <div class="ruler hidden" title="⇿">--><!--                        <img style="float: right; max-height: 16.5cm; position:absolute; margin-left: 0; margin-top: 0;" src="http://fontane-nb.dariah.eu:8080/js/pics/ruler-17cm.png" alt="Ruler 17cm"/>--><!--                    </div>--></xsl:otherwise>
            </xsl:choose>
        </xsl:if>
        <xsl:if test="contains(@n, 'inner')">
            <xsl:choose>
                <xsl:when test="(string-length(.) = 0) or (matches(string-join((descendant-or-self::tei:handShift[@new = '#Fontane']/text()), ' '), '^\s*?$')) and $rownum = ''"><!--                    <div class="ruler hidden" title="⇿">--><!--                        <img style="float: right; max-height: 3cm; position:absolute; margin-left: 0; margin-top: 10px;" src="http://fontane-nb.dariah.eu:8080/js/pics/ruler-17cm.png" alt="Ruler 17cm"/>--><!--                    </div>--></xsl:when>
                <xsl:otherwise><!--                    <div class="ruler hidden" title="⇿">--><!--                        <img style="float: right; max-height: 16.5cm; position:absolute; margin-left: 0; margin-top: 0;" src="http://fontane-nb.dariah.eu:8080/js/pics/ruler-17cm.png" alt="Ruler 17cm"/>
                    </div>
--></xsl:otherwise>
            </xsl:choose>
        </xsl:if>
        <xsl:if test="not(contains(@n, 'outer') or contains(@n, 'inner') or contains(@n, 'spine'))">
            <xsl:choose>
                <xsl:when test="(string-length(.) = 0) or (matches(string-join((descendant-or-self::tei:handShift[@new = '#Fontane']/text()), ' '), '^\s*?$')) and not(sum(descendant::zone[@uly][string-length(@uly) &gt; 0]/@uly) &gt; 3) and $rownum = ''"><!--                    <div class="ruler hidden" title="⇿">--><!--                        <img style="float: right; max-height: 3cm; position:absolute; margin-left: 0; margin-top: 10px;" src="http://fontane-nb.dariah.eu:8080/js/pics/ruler-17cm.png" alt="Ruler 17cm"/>--><!--                    </div>--></xsl:when>
                <xsl:otherwise><!--                    <div class="ruler hidden" title="⇿">--><!--                        <img style="float: right; max-height: 16.5cm; position:absolute; margin-left: 0; margin-top: 10px;" src="http://fontane-nb.dariah.eu:8080/js/pics/ruler-17cm.png" alt="Ruler 17cm"/>
                    </div>
--></xsl:otherwise>
            </xsl:choose>
        </xsl:if>
        <div class="surface-edi">
            <xsl:attribute name="id">
                <xsl:value-of select="@n"/>
            </xsl:attribute>
            <xsl:variable name="strglength" select="concat('\s{',string-length(.), '}')"/>
            <xsl:choose>
                <xsl:when test="@type='inner_front_cover'">
                    <xsl:if test="contains(descendant::zone[1]/@type, 'empty')">
                        <xsl:attribute name="style">
                            <xsl:value-of select="concat('min-height: 3cm', ';', 'width:', $surfaceWidth, $unit, ';')"/>
                        </xsl:attribute>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="@type='outer_front_cover'">
                    <xsl:if test="contains(descendant::zone[1]/@type, 'empty')">
                        <xsl:attribute name="style">
                            <xsl:value-of select="concat('min-height: 3cm', ';', 'width:', $surfaceWidth, $unit, ';')"/>
                        </xsl:attribute>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="@type='outer_back_cover'">
                    <xsl:if test="contains(descendant::zone[1]/@type, 'empty')">
                        <xsl:attribute name="style">
                            <xsl:value-of select="concat('min-height: 3cm', ';', 'width:', $surfaceWidth, $unit, ';')"/>
                        </xsl:attribute>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="@type='inner_back_cover'">
                    <xsl:if test="contains(descendant::zone[1]/@type, 'empty')">
                        <xsl:attribute name="style">
                            <xsl:value-of select="concat('min-height: 3cm', ';', 'width:', $surfaceWidth, $unit, ';')"/>
                        </xsl:attribute>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="@type='spine'">
                    <xsl:if test="contains(descendant::zone[1]/@type, 'empty')">
                        <xsl:attribute name="style">
                            <xsl:value-of select="concat('min-height: 3cm', ';', 'width:', $surfaceWidth, $unit, ';')"/>
                        </xsl:attribute>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="@type='fragment'">
                    <xsl:attribute name="style">
                        <xsl:value-of select="concat('min-height: 3cm', ';', 'width:', $surfaceWidth, $unit, ';')"/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="(string-length(.) = 0) or (matches(string-join((descendant-or-self::tei:handShift[@new = '#Fontane']/text()), ' '), '^\s*?$'))">
                    <xsl:choose>
                        <xsl:when test="sum(descendant::zone[@uly][string-length(@uly) &gt; 0]/@uly) &gt; 3">
                            <h1>
                                <xsl:value-of select="@n"/> (vakat+)</h1>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="style">
                                <xsl:if test="$rownum = ''">
                                    <xsl:value-of select="concat('min-height: 3cm', ';', 'width:', $surfaceWidth, $unit, ';')"/>
                                </xsl:if>
                                <xsl:if test="$rownum != ''">
                                    <xsl:value-of select="concat('min-height:',$surfaceHeight,$unit, ';','width:', $surfaceWidth, $unit, ';')"/>
                                </xsl:if>
                            </xsl:attribute>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise/>
            </xsl:choose>
            <xsl:apply-templates select=".//tei:zone[not(parent::zone)]"/><!--  <xsl:apply-templates select="./tei:zone[1]/following-sibling::tei:zone[string-length(@xml:id) = 0 and name(preceding-sibling::*[1]) != 'anchor']"/> -->
        </div><!-- präzisieren? -->
    </xsl:template>
    <xsl:template match="tei:line"><!-- EWZ getrennt von hinzuzufügendem Text platzieren? ("Zielstelle" des EWZ sonst unklar)-->
        <xsl:for-each select="./tei:add[@place = 'above']">
            <xsl:variable name="currentXpath">
                <xsl:text>/TEI/sourceDoc[1]/surface[@n='</xsl:text>
                <xsl:value-of select="ancestor::surface/@n"/>
                <xsl:text>']/</xsl:text>
                <xsl:for-each select="ancestor-or-self::*[name() != 'TEI'][name() != 'sourceDoc'][name() != 'surface']">
                    <xsl:value-of select="name()"/>
                    <xsl:text>[</xsl:text>
                    <xsl:value-of select="1+count(preceding-sibling::*[name() = current()/name()])"/>
                    <xsl:text>]/</xsl:text>
                </xsl:for-each>
            </xsl:variable>
            <xsl:variable name="margin-left">
                <xsl:value-of select="substring-after(substring-before(@style,'cm'),'margin-left:')"/>
            </xsl:variable>
            <span class="line above">
                <xsl:attribute name="style">
                    <xsl:text>position:absolute; font-size:smaller; margin-top:-1em; margin-left:</xsl:text>
                    <xsl:value-of select="$margin-left"/>
                    <xsl:text>cm</xsl:text>
                </xsl:attribute>
                <xsl:apply-templates/>
            </span>
            <span class="xpath hidden" style="position:absolute; font-size:smaller; margin-left:14cm;">
                <xsl:value-of select="$currentXpath"/>
            </span>
            <xsl:choose>
                <xsl:when test="contains(@rend,'caret:bow')"><!-- pos-left fehlt noch. -->
                    <xsl:variable name="width" select="number(substring-after(substring-before(@rend,'cm,pos-'),'caret:bow('))"/>
                    <svg width="{concat($width,'cm')}" height="1em" style="margin-left:{concat(string(number($margin-left)-0.5),'cm')}; margin-top:0.05em; stroke:black; stroke-width:2px; position:absolute">
                        <desc/>
                        <g alignment-baseline="baseline"/>
                        <line x1="0" y1="0.5em" x2="{concat($width,'cm')}" y2="0.5em"/>
                        <line x1="0" y1="0.5em" x2="0" y2="1em"/>
                        <line x1="0" y1="1em" x2="0.25cm" y2="1em"/>
                        <line x1="{concat($width,'cm')}" y1="0.5em" x2="{concat($width,'cm')}" y2="0"/>
                        <line x1="{concat($width,'cm')}" y1="0" x2="{concat(($width - 0.25),'cm')}" y2="0"/>
                    </svg>
                </xsl:when>
                <xsl:when test="contains(@rend,'caret:curved_V')">
                    <xsl:variable name="left" select="number(substring-after(substring-before(@rend,'cm,'),'caret:curved_V('))"/>
                    <xsl:variable name="right" select="number(substring-after(substring-before(@rend,'cm)'),'cm,'))"/>
                    <svg width="{concat(($left + $right),'cm')}" height="1em" style="margin-left:{concat(string(number($margin-left)-0.1),'cm')}; margin-top:0.5em; stroke:black; stroke-width:2px; position:absolute">
                        <desc/>
                        <g alignment-baseline="baseline"/>
                        <line x1="0" y1="0" x2="{concat($left,'cm')}" y2="0"/>
                        <line x1="{concat(($left),'cm')}" y1="0" x2="{concat(($left + 0.2),'cm')}" y2="0.75em"/>
                        <line x1="{concat(($left + 0.2),'cm')}" y1="0.75em" x2="{concat(($left),'cm')}" y2="0.75em"/>
                        <line x1="{concat(($left),'cm')}" y1="0.75em" x2="{concat(($left + 0.2),'cm')}" y2="0"/>
                        <line x1="{concat(($left + 0.2),'cm')}" y1="0" x2="{concat(($left + 0.2 + $right),'cm')}" y2="0"/>
                    </svg>
                </xsl:when>
                <xsl:when test="contains(@rend,'caret:V')">
                    <xsl:variable name="left" select="number(substring-after(substring-before(@rend,'cm,'),'caret:V('))"/>
                    <xsl:variable name="right" select="number(substring-after(substring-before(@rend,'cm)'),'cm,'))"/>
                    <svg width="{concat(($left + $right),'cm')}" height="0.75em" style="margin-left:{concat($margin-left,'cm')}; margin-top:0.3em; stroke:black; stroke-width:2px; position:absolute">
                        <desc/>
                        <g alignment-baseline="baseline"/>
                        <line x1="0" y1="0" x2="{concat($left,'cm')}" y2="0.75em"/>
                        <line x1="{concat(($left),'cm')}" y1="0.75em" x2="{concat(($left + $right),'cm')}" y2="0"/>
                    </svg>
                </xsl:when>
            </xsl:choose>
        </xsl:for-each>
        <span class="line">
            <xsl:apply-templates select="*[string-length(@place) = 0]"/><!-- präziser? (andere place-Werte als 'above' zu berücksichtigen?) -->
        </span>
    </xsl:template>
    <xsl:template match="tei:link"><!-- Zielstelle sollte unabhängig von der Platzierung des link-Tags sein. -->
        <xsl:variable name="target">
            <xsl:value-of select="substring-after(@target, ' #')"/>
        </xsl:variable><!--<div style="position:absolute;">-->
        <div class="link">
            <xsl:choose>
                <xsl:when test="//tei:anchor[@xml:id=$target]">
                    <xsl:apply-templates select="//tei:zone[preceding-sibling::tei:anchor[@xml:id=$target]]"/>
                </xsl:when>
                <xsl:when test="//tei:zone[@xml:id=$target]">
                    <xsl:apply-templates select="//tei:zone[@xml:id=$target]"/>
                </xsl:when>
            </xsl:choose>
        </div>
    </xsl:template>
    <xsl:template match="tei:gap">
        <xsl:choose>
            <xsl:when test="@unit='characters'">
                <span class="gap_chars" style="font-style:italic">
                    <xsl:text>&lt;</xsl:text>
                    <xsl:call-template name="x">
                        <xsl:with-param name="counter" select="1"/>
                        <xsl:with-param name="number" select="number(@quantity)"/>
                    </xsl:call-template>
                    <xsl:text>&gt;</xsl:text>
                </span>
            </xsl:when>
            <xsl:when test="@unit='words'">
                <span class="gap_words" style="font-style:italic">x---x</span>
            </xsl:when>
            <xsl:when test="@unit='cap_words'">
                <span class="gap_capwords" style="font-style:italic">X---x</span>
            </xsl:when>
            <xsl:when test="@unit='uncap_words'">
                <span class="gap_uncapwords" style="font-style:italic">x---x</span>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="x">
        <xsl:param name="counter"/>
        <xsl:param name="number"/>
        <xsl:if test="$counter &lt;= $number">
            <xsl:text>x</xsl:text><!-- Groß-/Kleinschreibung? -->
        </xsl:if>
        <xsl:if test="$counter &lt;= $number">
            <xsl:call-template name="x">
                <xsl:with-param name="counter" select="$counter+1"/>
                <xsl:with-param name="number" select="number(@quantity)"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template><!--    <xsl:template match="*[matches(., 'XXX1/2XXX')]">
        <xsl:for-each select="'XXX1/2XXX'">
            <table border="0" style="float: left; position: absolute; display: inline; font-size: 3mm; border-spacing: 0px;"><tbody><tr><td style="border-bottom:solid 1px">1</td></tr><tr><td>2</td></tr></tbody></table>            
        </xsl:for-each>
    </xsl:template>-->
    <xsl:template match="tei:unclear">
        <sup>?</sup>
        <xsl:apply-templates/>
        <sup>?</sup>
    </xsl:template><!--    <xsl:template match="tei:surface//tei:surface">
        <xsl:apply-templates />
    </xsl:template>-->
    <xsl:template match="tei:zone[descendant::text()[preceding::tei:handShift[@new]/@new = '#Fontane']]">
        <xsl:if test="descendant::text()[not(ancestor::tei:del)]">
            <xsl:element name="div"><!--  CLASS  -->
                <xsl:choose>
                    <xsl:when test="ancestor::zone/@type='figure'">
                        <xsl:attribute name="class">zone figuretext</xsl:attribute>
                    </xsl:when>
                    <xsl:when test="@type='figure'">
                        <xsl:attribute name="class">zone figure</xsl:attribute>
                    </xsl:when>
                    <xsl:when test="ancestor::surface/@n='outer_front_cover'">
                        <xsl:attribute name="class">zone outer_front</xsl:attribute>
                    </xsl:when>
                    <xsl:when test="contains(name(preceding-sibling::*[1]), 'addSpan') or contains(name(preceding-sibling::*[2]), 'addSpan')">
                        <xsl:attribute name="class">zone addSpan</xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:attribute name="class">zone</xsl:attribute>
                    </xsl:otherwise>
                </xsl:choose><!-- /CLASS -->
                <xsl:if test="@xml:id">
                    <xsl:attribute name="id">
                        <xsl:value-of select="@xml:id"/>
                    </xsl:attribute>
                </xsl:if>
                <xsl:attribute name="style"><!-- parent:SURFACE's attributes -->
                    <xsl:if test="parent::surface/@lrx and parent::surface/@ulx">
                        <xsl:value-of select="concat('width:',parent::surface/@lrx - parent::surface/@ulx,'cm; ')"/>
                    </xsl:if>
                    <xsl:if test="parent::surface/@lry and parent::surface/@uly">
                        <xsl:value-of select="concat('height:',parent::surface/@lry - parent::surface/@uly,'cm; ')"/>
                    </xsl:if>
                    <xsl:if test="parent::surface/@type='clipping'">background-color:#cdae92; </xsl:if>
                    <xsl:if test="@type='clipping'">background-color:#cdae92; </xsl:if>
                    <xsl:if test="parent::tei:surface/@uly">
                        <xsl:choose>
                            <xsl:when test="parent::tei:surface/@uly = ''">0</xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="concat('top:',parent::tei:surface/@uly - sum(parent::tei:surface/ancestor::*/@uly),'cm; ')"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:if>
                    <xsl:if test="@xml:id">
                        <xsl:variable name="id">
                            <xsl:value-of select="concat(' #',@xml:id)"/>
                        </xsl:variable>
                        <xsl:if test="//tei:link[contains(@target, $id)]/parent::tei:add[@style]">
                            <xsl:value-of select="concat('; ',//tei:link[contains(@target, $id)]/parent::tei:add/@style)"/>
                        </xsl:if>
                    </xsl:if>
                    <xsl:value-of select="@style"/>
                    <xsl:if test="contains(@rend, 'line-through-style:oblique')">; background:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgd2lkdGg9IjEwMHB4IiBoZWlnaHQ9IjEwMHB4IiBzdHlsZT0icG9zaXRpb246YWJzb2x1dGU7Ij4KICAgICAgICA8bGluZSB4MT0iMCIgeTE9IjAiIHgyPSIxMDAiIHkyPSIxMDAiIHN0cm9rZT0iZ3JheSIgc3Ryb2tlLXdpZHRoPSIycHgiLz4KPC9zdmc+) repeat-x; text-decoration:none;</xsl:if>
                    <xsl:if test="@type = 'figure' and not(descendant::tei:figure[@xml:id])"/>
                    <xsl:if test="@type = 'figure' and descendant::tei:figure[@xml:id]"/>
                </xsl:attribute>
                <xsl:if test="@type='figure'">
                    <xsl:element name="div">
                        <xsl:attribute name="class" select="'ulxuly'"/>
                        <xsl:attribute name="title" select="concat('x:', @ulx, '; y:', @uly)"/>
                    </xsl:element>
                    <xsl:element name="div">
                        <xsl:attribute name="class" select="'lrxlry'"/>
                        <xsl:attribute name="title" select="concat('x:', @lrx, '; y:',@lry)"/>
                    </xsl:element>
                </xsl:if>
                <xsl:apply-templates/>
            </xsl:element>
        </xsl:if>
    </xsl:template>
    <xsl:template match="tei:add[@place = '' or 'superimposed']">
        <xsl:choose>
            <xsl:when test="name(preceding-sibling::*[last()]) = 'del'"><!--                 <span class="add_del" style="position:absolute; margin-top:-.04cm;">
 -->
                <span class="add_del" style="margin-top:-.04cm;">
                    <xsl:attribute name="title">
                        <xsl:value-of select="preceding-sibling::del[1]/."/> überschrieben <xsl:value-of select="."/>
                    </xsl:attribute>
                    <xsl:value-of select="."/>
                </span>
                <xsl:choose><!-- präziser: -->
                    <xsl:when test="starts-with(following::*[1]/., ' ')"> &#160; </xsl:when>
                    <xsl:when test="string-length(.) &gt; 2"><!-- <xsl:text>   </xsl:text> --></xsl:when>
                    <xsl:otherwise><!-- kein Space -->
                        <xsl:text/><!-- Martins Space                        <xsl:text>　</xsl:text> -->
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="name(following-sibling::*[1]) = 'del'">
                <span style="position:absolute; margin-top:-.04cm">
                    <xsl:value-of select="."/>
                </span>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:del">
        <span class="del">
            <xsl:choose>
                <xsl:when test="name(preceding-sibling::*[1]) = 'add'">
                    <span style="position:absolute; margin-top:.04cm;">
                        <xsl:value-of select="."/>
                    </span><!-- präziser: -->
                    <xsl:choose>
                        <xsl:when test="string-length(.) &gt; 2"><!--  <xsl:text>   </xsl:text> --></xsl:when>
                        <xsl:otherwise><!-- kein Space -->
                            <xsl:text/><!-- Martins Space                  <xsl:text>　</xsl:text> -->
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="name(following-sibling::*[1]) = 'add'">
                    <span style="position:absolute; margin-top:.04cm">
                        <xsl:value-of select="."/>
                    </span>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates/>
                </xsl:otherwise>
            </xsl:choose>
        </span>
    </xsl:template>
    <xsl:template match="tei:figure[@type= 'line']">
        <hr/>
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:choice">
        <span class="abbr">
            <xsl:attribute name="title">
                <xsl:value-of select="descendant::tei:expan[1]"/>
            </xsl:attribute>
            <xsl:value-of select="descendant::tei:abbr[1]"/><!-- verallgemeinern -->
        </span>
    </xsl:template>
    <xsl:template match="tei:g">
        <xsl:if test="contains(@ref, 'mgem')">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" height="11" width="15">
                <xsl:element name="g">
                    <xsl:choose>
                        <xsl:when test="preceding::tei:handShift[1][@medium]/@medium = 'black_ink'">
                            <xsl:attribute name="style">
                                <xsl:text>fill:black; white-space: nowrap</xsl:text>
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:when test="preceding::tei:handShift[1][@medium]/@medium = 'brown_ink'">
                            <xsl:attribute name="style">
                                <xsl:text>fill:sienna; white-space: nowrap</xsl:text>
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:when test="preceding::tei:handShift[1][@medium]/@medium = 'blue_ink'">
                            <xsl:attribute name="style">
                                <xsl:text>fill:blue; white-space: nowrap</xsl:text>
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:when test="preceding::tei:handShift[1][@medium]/@medium = 'blue_pencil'">
                            <xsl:attribute name="style">
                                <xsl:text>fill:blue; white-space: nowrap</xsl:text>
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:when test="preceding::tei:handShift[1][@medium]/@medium = 'red_pencil'">
                            <xsl:attribute name="style">
                                <xsl:text>fill:red; white-space: nowrap</xsl:text>
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:when test="preceding::tei:handShift[1][@medium]/@medium = 'violet_ink'">
                            <xsl:attribute name="style">
                                <xsl:text>fill:#D16587; white-space: nowrap</xsl:text>
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="style">
                                <xsl:text>fill:#575757; white-space: nowrap</xsl:text>
                            </xsl:attribute>
                        </xsl:otherwise>
                    </xsl:choose>
                    <rect y="0" x="0" height="1" width="15"/>
                    <text style="font-size:20" y="11" x="0">m</text>
                </xsl:element>
            </svg>
        </xsl:if>
        <xsl:if test="contains(@ref, 'ngem')">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" height="11" width="9">
                <xsl:element name="g">
                    <xsl:choose>
                        <xsl:when test="preceding::tei:handShift[1][@medium]/@medium = 'black_ink'">
                            <xsl:attribute name="style">
                                <xsl:text>fill:black; white-space: nowrap</xsl:text>
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:when test="preceding::tei:handShift[1][@medium]/@medium = 'brown_ink'">
                            <xsl:attribute name="style">
                                <xsl:text>fill:brown; white-space: nowrap</xsl:text>
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:when test="preceding::tei:handShift[1][@medium]/@medium = 'blue_ink'">
                            <xsl:attribute name="style">
                                <xsl:text>fill:blue; white-space: nowrap</xsl:text>
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="style">
                                <xsl:text>fill:#575757; white-space: nowrap</xsl:text>
                            </xsl:attribute>
                        </xsl:otherwise>
                    </xsl:choose>
                    <rect y="0" x="0" height="1" width="9"/>
                    <text style="font-size:20" y="11" x="0">n</text>
                </xsl:element>
            </svg>
        </xsl:if>
    </xsl:template><!-- END stamp --><!--BEGIN Named Entity - Markup; now generic at all-->
    <xsl:template match="rs">
        <span>
            <xsl:attribute name="class">
                <xsl:value-of select="@type/string()"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="date">
        <span class="date">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="hi">
        <span class="hi">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="mod">
        <span>
            <xsl:attribute name="class">
                <xsl:value-of select="concat(normalize-space(concat(substring-after(preceding::handShift[1][@new]/@new, '#'), ' ',preceding::handShift[1][@medium]/@medium, ' ', preceding::handShift[1][@script]/@script)), ' mod')"/>
            </xsl:attribute>
            <xsl:apply-templates/>
            <xsl:if test="@instant = 'true'">↯</xsl:if>
        </span>
    </xsl:template><!--  mod -->
    <xsl:template match="mod[contains(@style, 'line-through') and not(@rend)]">
        <span class="linethrough">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="mod[contains(@style, 'line-through') and contains(@rend, 'blue')]">
        <span class="linethroughblue">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="mod[contains(@style, 'underline') and not(@rend)]">
        <span class="underline">
            <xsl:if test="contains(preceding::handShift[1]/@medium, 'brown')">
                <xsl:attribute name="style">border-color: brown;</xsl:attribute>
            </xsl:if>
            <xsl:if test="contains(preceding::handShift[1]/@medium, 'black')">
                <xsl:attribute name="style">border-color: black;</xsl:attribute>
            </xsl:if>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="mod[contains(@style, 'underline') and contains(@rend, 'wavy') and not(contains(@rend, 'blue_pencil'))]">
        <span class="underlinewavy">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="mod[contains(@style, 'underline') and contains(@rend, 'blue_pencil') and not(contains(@rend, 'wavy') and contains(@rend, 'bold'))]">
        <span class="underlinebluepencil">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="mod[contains(@style, 'underline') and contains(@rend, 'blue_pencil') and  contains(@rend, 'bold') and not(contains(@rend, 'wavy'))]">
        <span class="underlinebluepencilbold">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="mod[contains(@style, 'underline') and contains(@rend, 'blue_pencil') and  contains(@rend, 'wavy')]">
        <span class="underlinebluepencilwavy">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="mod[contains(@style, 'underline') and contains(@rend, 'blue_ink') and not(contains(@rend, 'wavy'))]">
        <span class="underlineblueink">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="tei:milestone[@unit='paragraph']">
        <br/>
    </xsl:template>
</xsl:stylesheet>