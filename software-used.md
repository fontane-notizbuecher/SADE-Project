# Software Used
We want to give an overview of all the software, frameworks and plugins used by
this project, because without all those packages provided under a free licence
we would never have come so far.

## Main Components
eXist - LGPL
Bootstrap - MIT
JQuery - MIT
GoldenLayout - MIT

## Plugins
Lazy Load 1.9.1 - MIT license

## Others
D3.js - BSD
Highland Template - WrapBootstrap Extended License
  * includes a variety of other free or licensed software
FontAwesome - Font: SIL OFL 1.1, CSS: MIT
Linux Libertine - SIL OFL 1.1
  * wurde um Geminationstypen ergänzt und wird unter dem Namen "Fontine" genutzt
  * Fontine - GPL-FE

## Further Software used in the project, provided by third party
TextGrid provided by SUB Göttingen
Mirador provided by TextGrid
IIIF Interface provided by TextGrid
git provided by GitHub
git provided by Gitlab hosted by GWDG
