xquery version "3.1";

let $log := util:log-system-out("Autostart: »Hello.«")
let $credentials :=
  if (available-environment-variables() = 'F_CREDENTIALS') then
    environment-variable('F_CREDENTIALS') => tokenize("\n")
  else
    file:read("/var/lib/textgrid/fontane/credentials.txt") => tokenize("\n");
let $configdoc := doc("/db/sade-projects/textgrid/config.xml")
let $credentialInjection :=
  for $line at $pos in $credentials
  return
  switch ($pos)
    case 1 return update replace $configdoc//param[@key="textgrid.user"]/text() with text {$line}
    case 2 return update replace $configdoc//param[@key="textgrid.password"]/text() with text {$line}
    case 3 return (
        (update replace $configdoc//param[@key="secret"]/text() with text {$line}),
        (update replace $configdoc//param[@key="sade.password"]/text() with text {$line}))
    case 4 return update replace $configdoc//param[@key="dokuwiki.user"]/text() with text {$line}
    case 5 return update replace $configdoc//param[@key="dokuwiki.password"]/text() with text {$line}
    default return false()
let $setpasswd := sm:passwd("admin", $credentials[3])

return
  true()