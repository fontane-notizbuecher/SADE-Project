(:~
 : Provides functions to execute and evaluate tests from XQSuite
 : @author Mathias Göbel
 : @see http://exist-db.org/exist/apps/doc/xqsuite.xml
 :)
xquery version "3.1";

module namespace tests="https://sade.textgrid.de/ns/tests/fontane";


(:~
 : Helper function to get all function names declared in the application.
 : Uses externally declared variable $target.
 :
 : @author Mathias Göbel
 : @since 3.1.0
 :)
declare function  tests:get-function-names($target)
as xs:string* {
for $modulePath in collection($target)/base-uri()[ends-with(., ".xqm")]
let $functions := inspect:module-functions( xs:anyURI($modulePath) ) (: QNames :)
return
  for $function in $functions
  let $functionQName := function-name($function)
  return
      prefix-from-QName($functionQName) || ":" || local-name-from-QName($functionQName)
};

(:~
 : Formats the test coverage output - mainly used by GitLab test coverage parser.
 :
 : @author Mathias Göbel
 : @since 3.1.0
 : @see https://docs.gitlab.com/ee/user/project/pipelines/settings.html#test-coverage-parsing
 :   :)
declare function  tests:format-test-rate-output($value as xs:decimal)
as xs:string {
    "coverage: " || ($value * 100) => format-number("###.00") || "%"
};

(:~
 : Returns the relative amount of tests for all function in this package
 : @param $tests A set of unit test results in the JUnit XML format
 :
 : @author Mathias Göbel
 : @since 3.1.0
 : @see https://www.ibm.com/support/knowledgecenter/en/SSQ2R2_14.1.0/com.ibm.rsar.analysis.codereview.cobol.doc/topics/cac_useresults_junit.html
 :  :)
declare function  tests:test-function-rate($tests as element(testsuites)*, $target)
as xs:string {
    (: is function cardinality an issue? thats why disstinct-value is used :)
    let $functionNames := ( tests:get-function-names($target)) => distinct-values() => count()
    let $testedFunctionNames := ($tests//testcase/string(@class)) => distinct-values() => count()
    let $coverage :=
        if($functionNames = 0) then 0 else $testedFunctionNames div $functionNames
    return
         tests:format-test-rate-output($coverage)
};

(:~
 : Returns the relative amount of successfull tests
 : @param $tests A set of unit test results in the JUnit XML format
 :
 : @author Mathias Göbel
 : @since 3.1.0
 : @see https://www.ibm.com/support/knowledgecenter/en/SSQ2R2_14.1.0/com.ibm.rsar.analysis.codereview.cobol.doc/topics/cac_useresults_junit.html
 :  :)
declare function  tests:test-success-rate($tests as element(testsuites)*)
as xs:string {
  let $allFailures := $tests//testsuite/@failures => sum()
  let $failsTriggered := $tests//testcase[@name=("FAIL", "fail")] => count()
  let $failures := $allFailures - $failsTriggered
  let $errors := $tests//testsuite/@errors => sum()
  let $pending := $tests//testsuite/@pending => sum()

  let $testsDone := $tests//testsuite/@tests => sum()

  let $testCoverage := ($testsDone - sum( ($failures, $errors, $pending) )) div $testsDone
  return
     tests:format-test-rate-output($coverage)
};
