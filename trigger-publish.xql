xquery version "3.1";

import module namespace etTransfo="http://fontane-nb.dariah.eu/etTransfo" at "/db/apps/SADE/modules/fontane/edited-text/etTransfo.xqm";
import module namespace tgconnect="http://textgrid.info/namespaces/xquery/tgconnect" at "/db/apps/textgrid-connect/tg-connect.xql";
import module namespace tgclient="http://textgrid.info/namespaces/xquery/tgclient" at "/db/apps/textgrid-connect/tgclient.xqm";

declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";
declare variable $configdoc := doc( "/db/sade-projects/textgrid/config.xml" );
declare variable $config := 
  map:merge( for $param in $configdoc//param
    return map:entry(string($param/@key), string($param))
  );
declare variable $sid :=
  tgclient:getSid(
    $config("textgrid.webauth"),
    $config("textgrid.authZinstance"),
    $config("textgrid.user"),
    $config("textgrid.password")
  );

let $uris := request:get-parameter("uris", "")
let $uris := if($uris = "") then  ( "textgrid:1zzdq" ) else $uris

let $password := request:get-parameter("pass", $config("sade.password"))

for $uri in tokenize($uris, ",")
let $startTime := util:eval("current-dateTime()")
where starts-with($uri, "textgrid:")
    let $response :=
      try {(
            tgconnect:publish($uri,
                        $sid,
                        "data",
                        "admin",
                        $password,
                        "textgrid",
                        ""),
            etTransfo:transform-single-nb(substring-after($uri, "textgrid:"))

    )} catch * { <error code="{$err:code}" timestamp="{util:eval("current-dateTime()")}">{$err:description}</error> }
return
    let $log := update insert ( <report uri="{$uri}" started="{$startTime}" finished="{util:eval("current-dateTime()")}">{$response}</report>) into //initlog/install/done
    return
        <report uri="{$uri}" start="{$startTime}" duration="{util:eval("current-dateTime()") - $startTime}">{$response}</report>
