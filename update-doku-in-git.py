#!/bin/python

from io import BytesIO
from lxml import etree
import requests

restUrl = 'https://fontane-nb.dariah.eu/rest/doku'

restGet = requests.get(restUrl)
tree = etree.parse( BytesIO(restGet.content) )

resources = tree.xpath('//exist:resource', namespaces=tree.getroot().nsmap)

for r in resources:
    target = r.attrib['name']
    loadUrl = f"{restUrl}/{target}"
    restGetTarget = requests.get(loadUrl)
    with open(target, mode='w') as localfile:
        localfile.write(restGetTarget.text)

print("done.")